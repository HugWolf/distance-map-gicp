\documentclass[10pt]{article}

\usepackage[linesnumbered, boxed]{algorithm2e}
\usepackage{gensymb}
\usepackage{amsmath}
\usepackage{url}

\title{Report on\\ \textbf{Improved Scan Matcher}}
\author{Antonio Cifonelli}
\date{September 15, 2017}

\begin{document}

	\maketitle
	
	\begin{abstract}
\noindent In this report we will present the work made to develop an improved scan matcher that takes advantage of the normals to a point. The normals will be used for: find a better correspondence pair than the usual ICP; reject outliers and/or incorrect correspondences; improve the information matrix.
	\end{abstract}
	
	\section{Problem Statement}
	\label{Sec:Intro}
	Our aim is to find the relative transformation between two consecutive scans and retrieve the robot pose. To achieve this goal we will employ the Generalized-ICP~\cite{GICP}; the general idea behind the ICP technique is to iteratively revise the transformation between a \textit{source} point cloud and the \textit{reference} one. The standard ICP schema is outlined in Alg.~(\ref{alg:SICP})
	
	\begin{algorithm}[H]
		\DontPrintSemicolon
		\SetKwFunction{FindClosestPointInB}{FindClosestPointInB}
		\Indentp{-0.9em}
		\KwData{Two point clouds $A=\{a_i\}$ and $B=\{b_i\}$; an initial transformation $T_0$}
		\KwResult{The correct transformation \textit{T} which aligns \textit{A} and \textit{B}}
		\Indentp{0.9em}
		$T\gets\ T_0$\;
		\While{not converged}{
			\For{$i\gets 1\ \KwTo\ N$}{
				$m_i\ \gets$ \FindClosestPointInB{$T\cdot a_i$}\;
				\eIf{$\mid\mid m_i - T\cdot a_i\mid\mid\leq d_{max}$}{$w_i\gets\ 1$}{$w_i\gets\ 0$}
			}
			$T\gets\underset{T}{\arg\!\min}\big\{\sum_i w_i\mid\mid m_i - T\cdot a_i\mid\mid^2\big\}$
		}
		\caption{Standard - ICP}
		\label{alg:SICP}
	\end{algorithm}
	\medskip
	\noindent where $d_{max}$ is the distance threshold, which accounts for the fact that some points will not have any correspondence in the second scan, and represents a real trade-off between convergence and accuracy. We will talk about this threshold later, in Section~\ref{Sec:Impl}.\\
	One of the most known variants of ICP is the \textit{Point-to-Plane} ICP that takes advantage of the surface normal information, adding robustness and accuracy to the standard ICP. The main improvement is the error function; line 11 of Alg.~(\ref{alg:SICP}) is modified to include the surface normal $\eta_i$ at $m_i$ (i.e. to project $(m_i - T\cdot a_i)$ onto the sub-space spanned by $\eta_i$) such that
	\begin{equation}
		\nonumber
		T\gets\underset{T}{\arg\!\min}\big\{\sum_i w_i\mid\mid\eta_i\cdot ( m_i - T\cdot a_i ) \mid\mid^2\big\}
	\end{equation}
	Generalized-ICP makes another step attaching a probabilistic model to this minimization step. The idea is that both point clouds $A$ and $B$ are drawn from an underlying sets of points $\hat{A}$ and $\hat{B}$ which generate $A$ and $B$ according to a Gaussian distribution. Applying this idea, the line 11 is modified as follows
	\begin{equation}
		T\gets\underset{\mathbf{T}}{\arg\!\min}\sum_i d_i^{(\mathbf{T})^T}(C_i^B + \mathbf{T}C_i^A\mathbf{T}^T)^{-1} d_i^{(\mathbf{T})},
		\label{eq:GICP_min}
	\end{equation}
	where $d_i^{(\mathbf{T})} = b_i - T\cdot a_i$ such that $\mid\mid m_i - T\cdot a_i \mid\mid\leq d_{max}$, $C_i^A$ and $C_i^B$ are covariance matrices associated with the measured points. The whole derivation can be found in~\cite{GICP} together with the proof that standard ICP and its \textit{Point-to-Plane} variant can be viewed as special cases of Generalized-ICP.
	
	\section{Resources and implementation}
	\label{Sec:Impl}
	The project is totally developed in C++ and requires Eigen~\cite{Eig} and OpenCV~\cite{Ocv} libraries to work correctly. Moreover you need CMake to compile the sources.
	\subsection{Cleaning the raw data}
	First of all we need some data to deal with; we chose to work with the CARMEN data log taken at University of Freiburg inside building 079 (a.k.a. AIS-Lab). This data set was obtained from the Robotics Data Set Repository (Radish)~\cite{Radish} and our thanks go to Cyrill Stachhniss for providing it. The log records data coming from odometry and the front laser sensor, plus several internal messages; in our development we make use only of the laser data. Each line of the complete log looks like the following\\
	\fbox{
		\begin{minipage}{\textwidth}
		FLASER 360 1.74 1.74 1.74 1.75 1.75 1.75 1.76 1.76 1.77 1.77 1.78 1.78 1.78 1.79 1.79 1.79 1.80 1.81 1.81 3.93 1.72 1.69 1.66 1.64 1.64 1.59 1.49 1.46 1.43 1.43 1.44 3.43 3.39 3.32 3.26 3.21 3.16 3.11 3.06 3.02 1.89 1.83 1.78 1.77 1.77 1.78 1.79 1.81 1.83 1.86 1.89 1.89 1.89 1.90 1.90 1.73 1.67 1.67 1.68 1.87 1.89 1.93 1.98 2.09 2.91 2.88 2.86 2.84 2.81 2.78 2.77 2.73 2.71 2.69 2.67 2.65 2.62 2.61 2.59 2.57 2.55 2.53 2.50 2.43 2.42 2.41 2.44 2.43 2.40 2.39 2.39 2.37 2.35 2.35 2.33 2.31 2.30 2.29 2.28 2.27 2.26 2.25 2.24 2.22 2.21 2.21 2.20 2.20 2.18 2.17 2.16 2.14 2.14 2.14 2.10 2.00 1.85 1.95 1.84 1.81 1.81 1.81 1.81 1.82 1.82 1.82 1.83 1.98 2.03 2.03 2.02 2.02 1.98 1.97 1.98 1.95 1.95 6.10 6.09 6.07 6.07 6.06 6.05 6.04 6.04 5.84 5.75 5.74 5.73 5.73 5.73 5.73 5.73 5.73 5.72 5.72 5.84 5.97 5.97 5.97 5.97 5.97 5.97 5.96 5.96 6.04 6.51 6.51 6.53 6.31 6.28 6.33 6.49 4.14 6.35 6.07 6.07 6.08 6.12 4.50 4.12 3.97 6.57 6.59 4.51 6.68 6.69 4.13 4.12 2.19 2.19 2.12 2.10 2.07 2.01 2.01 2.02 2.01 5.27 5.26 3.13 3.15 3.18 2.98 2.96 3.02 3.26 3.26 3.20 3.14 3.08 3.02 2.97 1.94 1.89 1.88 1.69 1.63 1.62 1.64 1.64 1.65 1.66 1.66 1.67 1.67 1.70 1.71 1.72 1.72 1.73 1.74 1.75 1.76 1.77 1.78 1.78 1.78 1.80 1.82 1.83 1.83 1.86 1.88 1.88 1.90 1.91 1.92 1.94 1.95 1.97 1.98 2.00 2.02 2.03 2.05 2.07 2.09 2.10 2.11 2.14 2.15 2.20 2.28 2.28 5.46 5.52 5.49 5.49 5.45 5.42 5.38 5.34 5.29 5.25 5.32 5.34 5.40 5.99 6.21 6.57 6.79 6.53 6.46 6.50 6.61 6.73 6.85 7.00 7.20 7.42 7.19 4.80 4.79 4.78 4.80 4.78 4.76 1.28 1.27 1.26 1.26 1.24 1.24 1.23 1.23 1.23 1.23 1.22 1.22 1.22 1.21 1.21 1.21 1.21 1.15 1.10 1.10 0.95 0.87 0.94 0.94 0.91 0.82 0.66 0.64 0.65 0.63 0.64 0.65 0.64 0.64 0.64 0.64 0.64 0.63 0.64 0.64 0.64 0.63 0.64 0.64 0.62 0.64 0.66 0.65 0.65 0.65 0.65 0.65 0.66 0.79 0.94 0.97 0.97 0.98 0.98 0.97 0.98 0.98 -1.208780 -0.139971 0.273777 -1.170270 -0.129157 0.273777 251.269183 magnum 0.067242
ODOM -1.170270 -0.129157 0.273777 0.000000 0.000000 0.000000 251.328607 magnum 0.068791
		\end{minipage}
	}\medskip\\
	so to filter the data log from headers, messages and undesired data we wrote a little bash script to purge the log in input; the ouput is always the ``\textit{cuttedLaser.log}" file which holds only the laser measurements. Now each line of the log counts for a single laser scan made of 360 measurements in meters (one measurement each half degree, from $0^{\degree}$ to $180^{\degree}$ in the robot reference frame).\\
	\fbox{
		\begin{minipage}{\textwidth}
		1.74 1.74 1.74 1.75 1.75 1.75 1.76 1.76 1.77 1.77 1.78 1.78 1.78 1.79 1.79 1.79 1.80 1.81 1.81 3.93 1.72 1.69 1.66 1.64 1.64 1.59 1.49 1.46 1.43 1.43 1.44 3.43 3.39 3.32 3.26 3.21 3.16 3.11 3.06 3.02 1.89 1.83 1.78 1.77 1.77 1.78 1.79 1.81 1.83 1.86 1.89 1.89 1.89 1.90 1.90 1.73 1.67 1.67 1.68 1.87 1.89 1.93 1.98 2.09 2.91 2.88 2.86 2.84 2.81 2.78 2.77 2.73 2.71 2.69 2.67 2.65 2.62 2.61 2.59 2.57 2.55 2.53 2.50 2.43 2.42 2.41 2.44 2.43 2.40 2.39 2.39 2.37 2.35 2.35 2.33 2.31 2.30 2.29 2.28 2.27 2.26 2.25 2.24 2.22 2.21 2.21 2.20 2.20 2.18 2.17 2.16 2.14 2.14 2.14 2.10 2.00 1.85 1.95 1.84 1.81 1.81 1.81 1.81 1.82 1.82 1.82 1.83 1.98 2.03 2.03 2.02 2.02 1.98 1.97 1.98 1.95 1.95 6.10 6.09 6.07 6.07 6.06 6.05 6.04 6.04 5.84 5.75 5.74 5.73 5.73 5.73 5.73 5.73 5.73 5.72 5.72 5.84 5.97 5.97 5.97 5.97 5.97 5.97 5.96 5.96 6.04 6.51 6.51 6.53 6.31 6.28 6.33 6.49 4.14 6.35 6.07 6.07 6.08 6.12 4.50 4.12 3.97 6.57 6.59 4.51 6.68 6.69 4.13 4.12 2.19 2.19 2.12 2.10 2.07 2.01 2.01 2.02 2.01 5.27 5.26 3.13 3.15 3.18 2.98 2.96 3.02 3.26 3.26 3.20 3.14 3.08 3.02 2.97 1.94 1.89 1.88 1.69 1.63 1.62 1.64 1.64 1.65 1.66 1.66 1.67 1.67 1.70 1.71 1.72 1.72 1.73 1.74 1.75 1.76 1.77 1.78 1.78 1.78 1.80 1.82 1.83 1.83 1.86 1.88 1.88 1.90 1.91 1.92 1.94 1.95 1.97 1.98 2.00 2.02 2.03 2.05 2.07 2.09 2.10 2.11 2.14 2.15 2.20 2.28 2.28 5.46 5.52 5.49 5.49 5.45 5.42 5.38 5.34 5.29 5.25 5.32 5.34 5.40 5.99 6.21 6.57 6.79 6.53 6.46 6.50 6.61 6.73 6.85 7.00 7.20 7.42 7.19 4.80 4.79 4.78 4.80 4.78 4.76 1.28 1.27 1.26 1.26 1.24 1.24 1.23 1.23 1.23 1.23 1.22 1.22 1.22 1.21 1.21 1.21 1.21 1.15 1.10 1.10 0.95 0.87 0.94 0.94 0.91 0.82 0.66 0.64 0.65 0.63 0.64 0.65 0.64 0.64 0.64 0.64 0.64 0.63 0.64 0.64 0.64 0.63 0.64 0.64 0.62 0.64 0.66 0.65 0.65 0.65 0.65 0.65 0.66 0.79 0.94 0.97 0.97 0.98 0.98 0.97 0.98 0.98
		\end{minipage}
	}\medskip\\
	You can find the script in attachment to this report under the name ``\textit{purgeData.sh}"; to clean a log file you have to enter the command ``\textit{./purgeData.sh carmen\_log\_file}" in a terminal window.\\
	The forthcoming step consist of conversion between this bunch of measurements and 2D points; assuming that the sensor reference frame coincides with the robot's one and that the sensor starts collecting measurements at $0^{\degree}$ following a counter-clockwise rotation around its z-axis, we can compute the point corresponding to a laser measurement as
	\begin{equation}
		\begin{pmatrix}
		x\\
		y
		\end{pmatrix}
		=
		ray\begin{pmatrix}
		\cos\theta\\
		\sin\theta
		\end{pmatrix}
	\end{equation}
	whit $ray$ equals to the measurement read from the log; $\theta\in \left[0^{\degree},180^{\degree}\right]$ is increased by half degree after each read. The outcome points are directly pushed into the point cloud structure and are ready for the next stage.
	\subsection{Distance Map and correspondences}
	We started with a chunk of data and ended up with a set of 2D points; the first dowel of our actual ICP algorithm is the computation of the correspondences by mean of the distance threshold $d_{max}$. The general function $FindClosestPointInB(\cdot)$ can be coded in different ways, the most common one is to iterate over $B$, for each point in $A$, searching for the closest point $m_i$ to $T\cdot a_i$; here instead we take advantage of the OpenCV library to compute a \textit{Distance Map}.\\
	Roughly speaking a Distance Map is a grid lookup table that we can pre-compute about the fixed point cloud; each cell of the grid holds the distance from the closest point and its identity. When the time to compute point correspondences comes, we can just query the grid. Different heuristics can be employed during the computation, we chose the gating one: we expand the Distance Map up to the maximum distance $d_{max}$.
	\subsubsection{Decimating points}
	If the sampled surfaces present oversampled areas the algorithm can be mislead towards an erroneous transformation; we can alleviate the issue sub-sampling such areas. With the Distance Map framework this step becomes really simple; we can just check the status of a grid cell and compute the centroid if it contains two or more points.
	\subsubsection{Filtering correspondences}
	Here we can directly employ the surface normals; once we have a putative correspondence on the Distance Map we can accept or reject it by mean of the normals; if they are under a certain error threshold we will accept the correspondence, otherwise not.
	\section{RANSAC and Least-Mean-Squares}
	\subsection{RANSAC}
	There is a whole literature on this argument, so we will not discuss it in detail here.\\
	The main points in our implementation are
	\begin{description}
		\item[Surface normal to a point in order to reduce searching time]: for a \\general 2D isometry we need at least 4 points, hence 2 point correspondences. Taking into account the normals we can search just for one correspondence and use each point normal to account for the remaining degrees of freedom;
		\item[Kabsch algorithm]: it is a method to compute the optimal rotation matrix between two sets of points. Once the rotation is performed, the translation can be estimated as the difference between the centroids of the two sets. We use it to compute the initial guess for the Least-Mean-Squares step.
	\end{description}
	Once we have the initial guess, we can continue with the Least-Mean-Squares. Please refers to Algorithm (2) in~\cite{Gris:15} for a complete outline.
	\section{Compilation and usage}
	You can easily compile running the $compile.sh$ bash script. To run the program
	\begin{equation}
		\begin{aligned}
		\nonumber
			&cd\quad build\\
			&./SM\_DistanceMap\quad logFile\quad resolution
		\end{aligned}
	\end{equation}
	where $logFile$ has the form of $cuttedLaser.log$ and $resolution$ indicates (in meters) the dimension of each pixel (e.g. $0.05 = 5\ cm$ ). The project comes with different demos to demonstrate the actual functionalities of the program; there is a total of five demos
	\begin{description}
		\item[Distance Map]: the demo will take the first scan from the input file and incrementally draw a distance map, with a $d_{max}$ raging from $0$ to $8\ m$ and a $resolution = 5\ cm$;
		\item[Kabsch]: it's a simple demo to demonstrate the algorithm. Four points (drawn in red) are placed at the corners of a square, the same points are then rotated and translated and saved in another set (drawn in blue). The demo will compute the transformation between these two simple sets and draw (in green) the outcome set coming from applying the computed transformation to the first set;
		\item[RANSAC]: the first and the second scan are read from the file. You can navigate the two point clouds respectively with $[w,\ s]$ and $[a,\ d]$ to pass through the sets forward or backward. Hitting the space bar made a correspondence between the two highlighted points, finally pressing ``Enter" you will run RANSAC which will compute an initial transformation.
		\item[Solver]: we read the first scan from the file to populate two different point clouds. The first one will be used to compute the Distance Map (with $d_{max}= 8\ m$), while the second can be moved as you prefer. To move around the second point cloud use $[w,\ s,\ a,\ d]$ for $[up,\ down,\ left,\ right]$ direction; use instead $[q,\ e]$ to rotate the point cloud about $10\degree$, $q$ for a clockwise rotation ($z$-axis pointing inside the screen), $e$ for a counter-clockwise one. Hitting the space bar you will compute the transformation between the two point clouds with an error up to the $resolution$.
		\item[Complete Test]: you can launch the whole project. We will iterate over the whole log file computing incrementally the relative transformations. The demo opens two windows: one to show the actual matching; the other to draw the complete map. Unfortunately the latter is not shown in the proper way.
	\end{description}
	
	\begin{thebibliography}{5}
	
		\bibitem{GICP} A.~V.~Segal, D.~Haehnel, S.~Thrun.
		``\textit{Generalized-ICP}''.\\ \textit{Robotics: science and systems}. Vol. 2. No. 4. 2009
		\bibitem{Gris:15} G.~Grisetti.
		``\textit{Notes on Least-Squares and SLAM - Draft Version}''. 2015.\\ \url{https://goo.gl/amGTfX}
		\bibitem{Radish} A.~Howard, N.~Roy.
		``\textit{The Robotics Data Set Repository (Radish)}''. 2003.\\
		\url{http://radish.sourceforge.net/}
		\bibitem{Eig} Eigen Library.\\ \url{http://eigen.tuxfamily.org/index.php?title=Main_Page}
		\bibitem{Ocv} OpenCV Library. \url{http://opencv.org/}
		
	\end{thebibliography}

\end{document}