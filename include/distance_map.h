#ifndef DISTANCE_MAP_H
#define DISTANCE_MAP_H

//#include "include/global.h"
#include "global.h"
#include <cfloat>
#include <queue>

struct MapCell{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    MapCell(){
        parent_row = 0;
        parent_col = 0;
        row = 0;
        col = 0;
        distance = INT32_MAX;
    }

    int parent_row;
    int parent_col;
    int row;
    int col;
    int distance;

    inline bool operator<(const MapCell& c) const{
        return c.distance < distance;
    }

    inline bool operator ()(const MapCell& c1, const MapCell& c2){
        return (c1.distance>c2.distance) ||
               (c1.distance==c2.distance && c1.row>c2.row) ||
               (c1.distance==c2.distance && c1.row==c2.row && c1.col>c2.col);
    }
};

typedef std::priority_queue<MapCell> MapQueue;
typedef Eigen::Matrix<MapCell, Eigen::Dynamic, Eigen::Dynamic> Map;

class DistanceMap{
    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
        int neighborhood(MapCell* neighbors, const int &row, const int &col, const int& rows, const int& cols);
        void compute(IndexImage& iImage, const float& density, const float& max_distance);
        void correspondences(const IndexImage& indexes, const Vector2dVector& points, const float& density, IndexSet& correspondences_set);
        void correspondences_withNormals(const IndexImage& indexes, const Vector2dVector &reference_points, const Vector2dVector& points, const float& density, IndexSet& correspondences_set);
        RGBImage getImage(const float& max_distance);

    protected:
        Map _distance_map;
        DistancesImage _distances_image;

    private:
        void pointNormal(const Eigen::Vector2d point, const Vector2dVector& points, Eigen::Vector2d& normal_vector, const int& neighbours);
};

#endif // DISTANCE_MAP_H
