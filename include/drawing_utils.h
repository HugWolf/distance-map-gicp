#ifndef DRAWING_UTILS_H
#define DRAWING_UTILS_H

//#include "include/global.h"
#include "global.h"

void drawPoints(RGBImage& dImage, const Vector2dVector& points, const float &density, cv::Scalar color);
void drawSelection(RGBImage& dImage, const Vector2dVector& points, const size_t &idx, const float& density, cv::Scalar color);
void drawCorrespondences(RGBImage& image, const IndexSet& indexes, const Vector2dVector& references, const Vector2dVector& points, const float& density);
void drawMap(RGBImage& image, const Vector2dVector& points, const Eigen::Vector2d& robot_position, const float& density);
void drawMap(RGBImage& image, const Vector2dVector& points, const Eigen::Isometry2d& T, const Eigen::Isometry2d &H, const float& density);

#endif // DRAWING_UTILS_H
