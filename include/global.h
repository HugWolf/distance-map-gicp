#ifndef GLOBAL_H
#define GLOBAL_H

#include <Eigen/Core>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <opencv2/opencv.hpp>
#include <set>

typedef std::vector<Eigen::Vector2d,EIGEN_ALIGNED_ALLOCATOR<Eigen::Vector2d>> Vector2dVector;

typedef std::pair<int, int> IndexPair;
typedef std::set<IndexPair> IndexSet;
typedef std::set<IndexPair>::iterator IndexSetIterator;

struct ExtPoint{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    ExtPoint(){
        storage_point.setZero(2,1);
        centroid.setZero(2,1);
        n_items = 0;
    }

    Eigen::Vector2d storage_point;
    Eigen::Vector2d centroid;
    int n_items;
};

struct NeighbourEntry{
    NeighbourEntry(){
        index = 0;
        error = 0.;
    }

    int index;
    double error;
    inline bool operator<(const NeighbourEntry& n) const{
        return error > n.error;
    }

    inline bool operator ()(const NeighbourEntry& n1, const NeighbourEntry& n2){
        return (n1.error>n2.error);
    }
};
typedef std::priority_queue<NeighbourEntry> NeighbourDistance;


typedef cv::Mat_<cv::Point3d> PointsImage;
typedef cv::Mat_<cv::Point3d> MapImage;
typedef cv::Mat_<ExtPoint> ExtPointsImage;
typedef cv::Mat_<ExtPoint> ExtMapImage;
typedef cv::Mat_<int> IndexImage;
typedef cv::Mat_<float> DistancesImage;
typedef cv::Mat_<cv::Vec3b> RGBImage;

inline Eigen::MatrixXd Jacobian(const Eigen::Vector3d& vec){
    Eigen::MatrixXd jac;
    jac.setZero(2,3);
    jac.col(0) << 1.         , 0.;
    jac.col(1) << 0.         , 1.;
    jac.col(2) << (-1*vec(1)), vec(0);
    return jac;
}

inline Eigen::Matrix2d R(const double& angle){
    Eigen::Matrix2d T;
    T.setZero(2,2);
    double cosine = std::cos(angle);
    double sine = std::sin(angle);
    T << cosine, -sine,
         sine, cosine;
    return T;
}

inline Eigen::Vector3d t2V(const Eigen::Isometry2d& T){
    double angle = std::atan2(T(1,0), T(0,0));
    return Eigen::Vector3d(T(0,2), T(1,2), angle);
}

inline Eigen::Matrix3d v2S(const Eigen::Vector3d& v){
    Eigen::Matrix3d T;
    T.setZero(3, 3);
    T << 0, -v(2), v(1),
         v(2), 0, -v(0),
         -v(1), v(0), 0;
    return T;
}

inline Eigen::Isometry2d v2T(const Eigen::Vector3d& v){
    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
    T.translation() = v.head(2);
    double cosine = std::cos(v(2));
    double sine = std::sin(v(2));
    T.linear() << cosine, -sine,
                  sine, cosine;
    return T;
}

inline Eigen::Isometry2d add(const Eigen::Isometry2d& X, Eigen::Vector3d& vec){
//    return v2T(vec)*X;
    return X*v2T(vec);
}

inline Eigen::Vector3d sub(const Eigen::Isometry2d& X1, const Eigen::Isometry2d& X2){
    return t2V(X1.inverse()*X2);
}

#endif // GLOBAL_H
