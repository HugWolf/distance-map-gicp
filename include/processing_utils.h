#ifndef PROCESSING_UTILS_H
#define PROCESSING_UTILS_H

#include "global.h"
#include <fstream>
#include <iostream>
#include <math.h>
#include <string.h>

inline void polarToEucledian(const float& ray, const float& theta, double& x, double& y){
    x = ray*std::cos(theta);
    y = ray*std::sin(theta);
}

void decimatePoints(PointsImage& pImage, IndexImage& iImage, Vector2dVector& reference_points, IndexSet& indexes, Vector2dVector& decimated_points, const float& density);
void decimatePoints(ExtPointsImage& pImage, Vector2dVector& reference_points, IndexSet& indexes, Vector2dVector& decimated_points, const float& density);
void decimatePoints(Vector2dVector &reference_points, IndexSet& indexes, Vector2dVector& decimated_points, const float& resolution);
void fillIndexes(IndexImage& iImage, const Vector2dVector& reference_points, const float &density);
int readCarmenData(std::ifstream& file, const std::string delimiter, Vector2dVector& readings);
int init();
void toCameraFrame(const Vector2dVector& points, Vector2dVector& transformed, const int& rows, const int& cols, const float &density);
void toWorldFrame(const Vector2dVector& points, Vector2dVector& transformed, const int& rows, const int& cols, const float &density);

void applyTransformation(const Eigen::Isometry2d& T, Vector2dVector& points);
void applyTransformation(const Eigen::Isometry2d& T, const Vector2dVector& points, Vector2dVector& transformed_points);


#endif // PROCESSING_UTILS_H
