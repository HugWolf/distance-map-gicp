#ifndef RANSAC_H
#define RANSAC_H

#include "global.h"
#include "solver.h"
#include <Eigen/Eigenvalues>
#include <Eigen/SVD>
#include <random>
#include <vector>

typedef Eigen::Matrix<double, 3, Eigen::Dynamic> DLT2D_Mat;
typedef Eigen::Transform<double, 2, Eigen::Affine> DLT2D_Transform2d;
typedef std::vector<IndexPair> CorrsVector;

struct HomographyEntry{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    HomographyEntry(){
        inliers = 0;
        std_dev = 0.;
        Eigen::Isometry2d I = Eigen::Isometry2d::Identity();
        h = Eigen::Map<Eigen::Matrix<double, 9, 1>>(I.data());
    }

    int inliers;
    double std_dev;
    Eigen::VectorXd h;
    inline bool operator<(const HomographyEntry& e) const{
        return inliers < e.inliers;
    }

    inline bool operator ()(const HomographyEntry& e1, const HomographyEntry& e2){
        return (e1.inliers>e2.inliers);
    }
};
typedef std::priority_queue<HomographyEntry> HomographyQueue;

class ransac{

    std::knuth_b generator;
    std::uniform_int_distribution<int> distribution;
    HomographyQueue q;
    solver S;

    public:
        Eigen::Isometry2d best_H;
        void compute(const Vector2dVector& references, const Vector2dVector& points, const IndexSet& correspondences_set, const int& n_point_correspondences, const int &use_normals, const double &threshold, const double& probability=0.99);
        void DLT2D(const DLT2D_Mat& src, const DLT2D_Mat& dst, const int& n_point_correspondences, DLT2D_Transform2d &H);
        void Kabsch(const DLT2D_Mat& src, const DLT2D_Mat& dst, DLT2D_Transform2d &H);

    private:
        void scalePoints(const DLT2D_Mat& points, DLT2D_Transform2d& T, const int &searchIsometry);
        void scalePointsKabsch(const DLT2D_Mat& points, Eigen::MatrixXd& scaled_points, Eigen::Vector2d& centroid);
        void arrangeEqsSet(const DLT2D_Mat& reference, const DLT2D_Mat& points, const int& n_point_correspondences, Eigen::MatrixXd& A);
        inline bool checkColinearity(const Eigen::Vector3d& vec1, const Eigen::Vector3d& vec2){
            double det = vec1.dot(vec2);
            return det==0;
        }
        inline Eigen::VectorXd initLine(const Eigen::Vector3d& vec1, const Eigen::Vector3d& vec2){
            Eigen::Isometry2d skew = v2T(vec1);
            Eigen::Vector3d line = skew*vec2;
            return line;
        }
        inline double symmTransfError(const Eigen::Vector2d& vec1, const Eigen::Vector2d& vec2, const DLT2D_Transform2d& H){
            Eigen::Vector2d vec11 = H*vec1;
            Eigen::Vector2d vec21 = H.inverse()*vec2;
            double err = (vec1 - vec21).squaredNorm() + (vec11 - vec2).squaredNorm();
//            std::cerr << "vec1 = " << std::endl << vec1 << std::endl << "vec2 = " << std::endl << vec2 << std::endl << "H_tilde = " << std::endl << H.matrix() << std::endl << "error = " << err << std::endl;
            return err;
        }
};

#endif // RANSAC_H
