#ifndef SOLVER_H
#define SOLVER_H

#include "global.h"
#include <Eigen/Cholesky>

class solver{
    public:
        void standardMatch(const Vector2dVector& reference, const Vector2dVector& points, const IndexSet& correspondences_set, Eigen::Isometry2d &Guess, const double& threshold, const int& maxIter);
        void standardMatchLM(const Vector2dVector& reference, const Vector2dVector& points, const IndexSet& correspondences_set, Eigen::Isometry2d &Guess, const double &epsilon, const int& maxIter);
        void generalizedMatchLM(const Vector2dVector& references, const Vector2dVector& points, const IndexSet& correspondences_set, Eigen::Isometry2d &Guess, const double& epsilon, const int& maxIter);
        void pointNormal(const Eigen::Vector2d point, const Vector2dVector& points, Eigen::Vector2d& normal_vector, const int& neighbours);
    private:
        void pointCovariance(const Eigen::Vector2d point, const Vector2dVector& points, Eigen::Matrix2d& covariance, const int& neighbours, const double& belief);
};

#endif // SOLVER_H
