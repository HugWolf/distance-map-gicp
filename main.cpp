#include "include/distance_map.h"
#include "include/drawing_utils.h"
#include "include/global.h"
#include "include/processing_utils.h"
#include "include/ransac.h"

#include <math.h>

using namespace std;
using namespace cv;

int DMdemo(ifstream& logFile, const double& density){
    RGBImage image;
    image.create(480, 640);
    image = 0;
    IndexImage idxsImage;
    idxsImage.create(480, 640);
    idxsImage = -1;

    Vector2dVector reference_points;
    Vector2dVector decimated_points;

    IndexSet idxsSet;


    namedWindow("Distance Map Demo");

    readCarmenData(logFile, string(" "), reference_points);

    Vector2dVector visible_points;
    toCameraFrame(reference_points, visible_points, 480, 640, density);
    decimatePoints(visible_points, idxsSet, decimated_points, density);

    DistanceMap map = DistanceMap();
    fillIndexes(idxsImage, decimated_points, density);
    double distance = 0.;
    map.compute(idxsImage, density, distance);
    image = map.getImage(distance);
    imshow("Distance Map Demo", image);
    cerr << "Press any key to start!\n";
    waitKey(0);
    while(distance<8.){
        idxsImage = -1;
        fillIndexes(idxsImage, decimated_points, density);
        map.compute(idxsImage, density, distance);
        image = map.getImage(distance);
        imshow("Distance Map Demo", image);
        waitKey(10);
        distance += .1;
    }
    cerr << "Press Q to quit!\n";
    char key = 0;
    while(tolower(key)!='q') key = waitKey(0);
    destroyWindow("Distance Map Demo");
    return 0;
}

int DLT2Ddemo(const double& density){
    ransac r = ransac();
    RGBImage image;
    image.create(480, 640);
    image = 0;
    Eigen::Isometry2d H(Eigen::Isometry2d::Identity());
    H.translation() = Eigen::Vector2d(320*density, 240*density);
    Vector2dVector SRC;
    Vector2dVector DST;
    DLT2D_Transform2d T;
    DLT2D_Mat src;
    src.setZero(3, 4);
    src << 1, 0, -1, 0,
           0, 1, 0, -1,
           1, 1, 1, 1;
    DLT2D_Mat dst;
    dst.setZero(3, 4);
    dst = Eigen::Translation2d(Eigen::Vector2d(5., 0.))*Eigen::Rotation2Dd((45*M_PI)/180.)*src;
    src = H*src;
    dst = H*dst;
    for(int i=0; i<src.cols(); ++i){
        SRC.push_back(src.col(i).head(2));
    }
    for(int i=0; i<src.cols(); ++i){
        DST.push_back(dst.col(i).head(2));
    }
    src = H.inverse()*src;
    dst = H.inverse()*dst;
    namedWindow("Direct Linear Method - 2D");
//    cerr << "dst = \n" << endl << dst << endl;
//    dst << 1, 0, -1, 0,
//           1, 2, 1, 0,
//           1, 1, 1, 1;
//    r.DLT2D(src, dst, 4, T);
    drawPoints(image, SRC, density, cv::Scalar(0, 0, 255));
    drawPoints(image, DST, density, cv::Scalar(255, 0, 0));
    imshow("Direct Linear Method - 2D", image);
    r.Kabsch(src, dst, T);
    cout << "[DLT] Isometry found = \n" << T.matrix() << endl;
    cerr << "T*src = \n" << T*src << endl;
    cerr << "T^{-1}*dst = \n" << T.inverse()*dst << endl;
    applyTransformation(Eigen::Isometry2d(H.inverse().matrix()), SRC);
    applyTransformation(Eigen::Isometry2d(T.matrix()), SRC);
    applyTransformation(Eigen::Isometry2d(H.matrix()), SRC);
    cerr << "Press any key to continue\n" << endl;
    waitKey(0);
    drawPoints(image, SRC, density, cv::Scalar(0, 255, 0));
    imshow("Direct Linear Method - 2D", image);
    cerr << "Press Q to quit!\n";
    char key = 0;
    while(tolower(key)!='q') key = waitKey(0);
    destroyWindow("Direct Linear Method - 2D");
    return 0;
}

int RANSACdemo(ifstream& logFile, const double& density){
    RGBImage image;
    image.create(480, 640);
    image = 0;

    Vector2dVector reference_points;
    Vector2dVector points;
    Vector2dVector decimated_points;
    Vector2dVector transformed_decimated_points;

    IndexSet idxsSet;

    ExtPointsImage pointsImage;
    ExtPointsImage transformed_pointsImage;
    pointsImage.create(480, 640);
    transformed_pointsImage.create(480, 640);

    namedWindow("RANSAC Demo");

    readCarmenData(logFile, string(" "), reference_points);
    readCarmenData(logFile, string(" "), points);

    Vector2dVector visible_points;
    toCameraFrame(reference_points, visible_points, 480, 640, density);
    decimatePoints(visible_points, idxsSet, decimated_points, density);
//    decimatePoints(pointsImage, visible_points, idxsSet, decimated_points, density);
    visible_points.clear();
    idxsSet.clear();
//    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
//    T.translation() = Eigen::Vector2d(5., 0.);
//    T.linear() = R(0.785);
//    applyTransformation(T, reference_points);
    toCameraFrame(points, visible_points, 480, 640, density);
    decimatePoints(visible_points, idxsSet, transformed_decimated_points, density);
//    decimatePoints(transformed_pointsImage, visible_points, idxsSet, transformed_decimated_points, density);
    ransac r = ransac();


    char key=0;
    const char ENTER_key='\n';

    drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));;
    drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));

    imshow("RANSAC Demo", image);


//    toWorldFrame(decimated_points, transformed_points, 480, 640, density);

//    toCameraFrame(transformed_points, transformed_decimated_points, 480, 640, density);
//    decimatePoints(transformed_pointsImage,visible_points, idxsSet, transformed_decimated_points, density);

//    idxsSet.clear();
//    drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
    idxsSet.clear();
    size_t idx_ref = 0;
    size_t idx_point = 0;
    int num_correspondences = 0;

    cerr << "Correspondences selected #" << num_correspondences << endl;
    drawSelection(image, decimated_points, idx_ref, density, cv::Scalar(0, 215, 255));
    drawSelection(image, transformed_decimated_points, idx_point, density, cv::Scalar(255, 0, 143));
    while(key!=ENTER_key){
        switch (tolower(key)) {
            case 'w':{
                ++idx_ref;
                idx_ref = (idx_ref>decimated_points.size()-1) ? 0:idx_ref;

                break;
            }
            case 's':{
                --idx_ref;
                idx_ref = (idx_ref<0) ? decimated_points.size()-1:idx_ref;
                break;
            }
            case 'a':{
                ++idx_point;
                idx_point = (idx_point>transformed_decimated_points.size()-1) ? 0:idx_point;
                break;
            }
            case 'd':{
                --idx_point;
                idx_point = (idx_point<0) ? transformed_decimated_points.size()-1:idx_point;

                break;
            }
            case ' ':{
//                idxsSet.insert(make_pair(idx_point, idx_ref));
                idxsSet.insert(make_pair(idx_ref, idx_point));
                ++num_correspondences;
                cerr << "Correspondences selected #" << num_correspondences << endl;
                break;
            }
            default:{
                break;
            }
        }
        image.create(480, 640);
        image = 0;
        drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
        drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
        drawSelection(image, decimated_points, idx_ref, density, cv::Scalar(0, 215, 255));
        drawSelection(image, transformed_decimated_points, idx_point, density, cv::Scalar(255, 0, 143));
        drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
        imshow("RANSAC Demo", image);
        key = waitKey(0);
    }
    reference_points.clear();
//    transformed_points.clear();
    points.clear();
    toWorldFrame(decimated_points, reference_points, 480, 640, density);
//    toWorldFrame(transformed_decimated_points, transformed_points, 480, 640, density);
    toWorldFrame(transformed_decimated_points, points, 480, 640, density);
//    r.compute(reference_points,transformed_points,idxsSet,num_correspondences,0.25);
    r.compute(reference_points, points,idxsSet,num_correspondences, 0, density);
    Eigen::Isometry2d H = r.best_H;
    applyTransformation(H, reference_points);
    decimated_points.clear();
    transformed_decimated_points.clear();
    image.create(480, 640);
    image = 0;
    toCameraFrame(reference_points, decimated_points, 480, 640, density);
//    toCameraFrame(transformed_points, transformed_decimated_points, 480, 640, density);
    toCameraFrame(points, transformed_decimated_points, 480, 640, density);
    drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
    drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
    imshow("RANSAC Demo", image);
    cerr << "Press Q to quit!\n";
    key = 0;
    while(tolower(key)!='q') key = waitKey(0);
    destroyWindow("RANSAC Demo");

    logFile.close();
    return 0;
}

//int SOLVERdemo(ifstream& logFile, const double& density){
//    RGBImage image;
//    image.create(480, 640);

//    Vector2dVector reference_points;
//    Vector2dVector points;
//    Vector2dVector decimated_points;
//    Vector2dVector transformed_decimated_points;

//    IndexSet idxsSet;
//    IndexImage idxsImage;
//    ExtPointsImage pointsImage;
//    ExtPointsImage transformed_pointsImage;

//    namedWindow("SOLVER Demo");

//    readCarmenData(logFile, string(" "), reference_points);
//    readCarmenData(logFile, string(" "), points);

////    Vector2dVector transformed_points;
//    Vector2dVector visible_points;
////    pointsImage.create(480, 640);
////    transformed_pointsImage.create(480, 640);
//    toCameraFrame(reference_points, visible_points, 480, 640, density);
//    decimatePoints(visible_points, idxsSet, decimated_points, density);
////    decimatePoints(pointsImage, visible_points, idxsSet, decimated_points, density);
//    visible_points.clear();
//    idxsSet.clear();
//    toCameraFrame(points, visible_points, 480, 640, density);
////    for (Vector2dVector::iterator it = reference_points.end(); it != reference_points.begin(); --it)
////        points.push_back(*it);
//    decimatePoints(visible_points, idxsSet, transformed_decimated_points, density);
////    decimatePoints(transformed_pointsImage, visible_points, idxsSet, transformed_decimated_points, density);

//    DistanceMap map = DistanceMap();
//    ransac r = ransac();

//    char key=0;

////    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
////    T.translation() = Eigen::Vector2d(.5, 0.);
////    T.linear() = R((10*M_PI)/180.);
////    toWorldFrame(decimated_points, transformed_points, 480, 640, density);
////    applyTransformation(T, transformed_points);
////    toCameraFrame(transformed_points, transformed_decimated_points, 480, 640, density);

//    idxsImage.create(480, 640);
//    idxsImage = -1;
//    fillIndexes(idxsImage, transformed_decimated_points, density);
//    map.compute(idxsImage, density, 2.24);
////    image = map.getImage(2.24);
////    drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
////    drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
////    imshow("SOLVER Demo", image);

//    idxsSet.clear();
//    map.correspondences(idxsImage, decimated_points, density, idxsSet);
//    image = map.getImage(2.24);
//    drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
//    drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
//    drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
//    imshow("SOLVER Demo", image);
//    int num_correspondences = 5;
//    double error = transformed_decimated_points.size();
//    cerr << "#Correspondences = " << idxsSet.size() << endl;
//    waitKey(0);
//    while(error>0.05){
//        image = map.getImage(2.24);
//        drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
//        drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
//        drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
//        imshow("SOLVER Demo", image);
//        reference_points.clear();
////        transformed_points.clear();
//        points.clear();
//        toWorldFrame(decimated_points, reference_points, 480, 640, density);
////        toWorldFrame(transformed_decimated_points, transformed_points, 480, 640, density);
//        toWorldFrame(transformed_decimated_points, points, 480, 640, density);
////        r.compute(reference_points,transformed_points,idxsSet,num_correspondences,0.25);
//        r.compute(reference_points,points,idxsSet,num_correspondences, 0, 0.25);
//        Eigen::Isometry2d H = r.best_H;
//        applyTransformation(H, reference_points);
//        decimated_points.clear();
//        transformed_decimated_points.clear();
//        toCameraFrame(reference_points, decimated_points, 480, 640, density);
////        toCameraFrame(transformed_points, transformed_decimated_points, 480, 640, density);
//        toCameraFrame(points, transformed_decimated_points, 480, 640, density);
//        idxsSet.clear();
//        map.correspondences(idxsImage, decimated_points, density, idxsSet);
//        image = map.getImage(2.24);
//        drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
//        drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
//        drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
//        imshow("SOLVER Demo", image);
//        error = 0.;
//        for(IndexSetIterator it=idxsSet.begin(); it!=idxsSet.end(); ++it){
//            IndexPair p = *it;
////            Eigen::Vector2d v1 = decimated_points[p.second];
////            Eigen::Vector2d v2 = transformed_decimated_points[p.first];
//            Eigen::Vector2d v1 = decimated_points[p.first];
//            Eigen::Vector2d v2 = transformed_decimated_points[p.second];
//            error += (v1 - v2).norm();
//        }
//        error /= idxsSet.size();
//        cerr << "Error = " << error << endl;
//        cerr << "#Correspondences = " << idxsSet.size() << endl;
//        waitKey(500);
//    }
////    transformed_points.clear();
////    transformed_points.resize(reference_points.size());
////    copy(reference_points.begin(), reference_points.end(), transformed_points.begin());
////    transformed_decimated_points.clear();
//    cerr << "Press Q to quit!\n";
//    key = 0;
//    while(tolower(key)!='q') key = waitKey(0);
//    destroyWindow("SOLVER Demo");
////    }

//    logFile.close();
//    return 0;
//}

int SOLVERdemo(ifstream& logFile, const double& density){
    RGBImage image;
    image.create(480, 640);

    Vector2dVector reference_points;
    Vector2dVector points;
    Vector2dVector visible_points;
    Vector2dVector decimated_points;
    Vector2dVector transformed_decimated_points;

    IndexSet idxsSet;
    IndexImage idxsImage;

    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());

    char key=0;
    const char ESC_key=27;
    double delta = .1;
    double angle = (10*M_PI)/180.;
    double max_distance = 8.;
    Eigen::Isometry2d rot(Eigen::Isometry2d::Identity());
    rot.linear() = R(angle);

    namedWindow("SOLVER Demo");

    readCarmenData(logFile, string(" "), reference_points);

    toCameraFrame(reference_points, visible_points, 480, 640, density);
    decimatePoints(visible_points, idxsSet, decimated_points, density);
    reference_points.clear();
    toWorldFrame(decimated_points, reference_points, 480, 640, density);
    points.resize(decimated_points.size());
    copy(decimated_points.begin(), decimated_points.end(), points.begin());

    DistanceMap map = DistanceMap();
    ransac r = ransac();

    idxsImage.create(480, 640);
    idxsImage = -1;
    fillIndexes(idxsImage, decimated_points, density);
    map.compute(idxsImage, density, max_distance);
    image = map.getImage(max_distance);
    drawPoints(image, decimated_points, density, cv::Scalar(0, 0, 255));
    drawPoints(image, points, density, cv::Scalar(255, 0, 0));
    idxsSet.clear();
    map.correspondences(idxsImage, points, density, idxsSet);
    drawCorrespondences(image, idxsSet, decimated_points, points, density);
    imshow("SOLVER Demo", image);
    waitKey(10);

    while(key!=ESC_key){
        image = map.getImage(max_distance);
        T.setIdentity();
        switch(key){
        case 'w': {
            T.translation().y() -= delta;
            break;
        }
        case 's': {
            T.translation().y() += delta;
            break;
        }
        case 'a': {
            T.translation().x() -= delta;
            break;
        }
        case 'd': {
            T.translation().x() += delta;
            break;
        }
        case 'q': {
            Eigen::Vector3d rotVec = t2V(rot);
            T= add(T, rotVec);
            break;
        }
        case 'e': {
            Eigen::Vector3d subVec = sub(rot, T);
            T = v2T(subVec);
            break;
        }
        case ' ':{
            int num_correspondences = 5;
            double error = points.size();
            int count = 0;
            while(error>0.05 && count<150){
                image = map.getImage(max_distance);
                drawPoints(image, decimated_points, density, cv::Scalar(0, 0, 255));
                drawPoints(image, points, density, cv::Scalar(255, 0, 0));
                drawCorrespondences(image, idxsSet, decimated_points, points, density);
                imshow("SOLVER Demo", image);
                waitKey(10);
                visible_points.clear();
                toWorldFrame(points, visible_points, 480, 640, density);
                r.compute(visible_points,reference_points,idxsSet,num_correspondences, 0, 0.25);
                Eigen::Isometry2d H = r.best_H;
                applyTransformation(H, visible_points);
                points.clear();
                toCameraFrame(visible_points, points, 480, 640, density);
                idxsSet.clear();
                map.correspondences(idxsImage, points, density, idxsSet);
                image = map.getImage(max_distance);
                drawPoints(image, decimated_points, density, cv::Scalar(0, 0, 255));
                drawPoints(image, points, density, cv::Scalar(255, 0, 0));
                drawCorrespondences(image, idxsSet, decimated_points, points, density);
                imshow("SOLVER Demo", image);
                waitKey(10);
                error = 0.;
                for(IndexSetIterator it=idxsSet.begin(); it!=idxsSet.end(); ++it){
                    IndexPair p = *it;
        //            Eigen::Vector2d v1 = decimated_points[p.second];
        //            Eigen::Vector2d v2 = transformed_decimated_points[p.first];
                    Eigen::Vector2d v1 = points[p.first];
                    Eigen::Vector2d v2 = decimated_points[p.second];
                    error += (v1 - v2).norm();
                }
                error /= idxsSet.size();
                cerr << "Error = " << error << endl;
                cerr << "#Correspondences = " << idxsSet.size() << endl;
                ++count;
                waitKey(10);
            }
            break;
        }
        default: break;
        }
//        cerr << "T = \n" << T.matrix() << endl;
//        cerr << "Translation = \n" << T.translation() << endl;
//        cerr << "Angle = " << (atan2(T(1,0), T(0,0))*180)/M_PI << endl;
        visible_points.clear();
        toWorldFrame(points, visible_points, 480, 640, density);
        applyTransformation(T, visible_points);
        points.clear();
        toCameraFrame(visible_points, points, 480, 640, density);
        idxsSet.clear();
        map.correspondences(idxsImage, points, density, idxsSet);
        drawPoints(image, decimated_points, density, cv::Scalar(0, 0, 255));
        drawPoints(image, points, density, cv::Scalar(255, 0, 0));
        drawCorrespondences(image, idxsSet, decimated_points, points, density);
        imshow("SOLVER Demo", image);
        key = waitKey(0);
    }

//    idxsSet.clear();
//    map.correspondences(idxsImage, decimated_points, density, idxsSet);
//    image = map.getImage(2.24);
//    drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
//    drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
//    drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
//    imshow("SOLVER Demo", image);
//    int num_correspondences = 5;
//    double error = transformed_decimated_points.size();
//    cerr << "#Correspondences = " << idxsSet.size() << endl;
//    waitKey(0);
//    while(error>0.05){
//        image = map.getImage(2.24);
//        drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
//        drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
//        drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
//        imshow("SOLVER Demo", image);
//        reference_points.clear();
////        transformed_points.clear();
//        points.clear();
//        toWorldFrame(decimated_points, reference_points, 480, 640, density);
////        toWorldFrame(transformed_decimated_points, transformed_points, 480, 640, density);
//        toWorldFrame(transformed_decimated_points, points, 480, 640, density);
////        r.compute(reference_points,transformed_points,idxsSet,num_correspondences,0.25);
//        r.compute(reference_points,points,idxsSet,num_correspondences, 0, 0.25);
//        Eigen::Isometry2d H = r.best_H;
//        applyTransformation(H, reference_points);
//        decimated_points.clear();
//        transformed_decimated_points.clear();
//        toCameraFrame(reference_points, decimated_points, 480, 640, density);
////        toCameraFrame(transformed_points, transformed_decimated_points, 480, 640, density);
//        toCameraFrame(points, transformed_decimated_points, 480, 640, density);
//        idxsSet.clear();
//        map.correspondences(idxsImage, decimated_points, density, idxsSet);
//        image = map.getImage(2.24);
//        drawPoints(image, decimated_points, density, cv::Scalar(255, 0, 0));
//        drawPoints(image, transformed_decimated_points, density, cv::Scalar(0, 0, 255));
//        drawCorrespondences(image, idxsSet, transformed_decimated_points, decimated_points, density);
//        imshow("SOLVER Demo", image);
//        error = 0.;
//        for(IndexSetIterator it=idxsSet.begin(); it!=idxsSet.end(); ++it){
//            IndexPair p = *it;
////            Eigen::Vector2d v1 = decimated_points[p.second];
////            Eigen::Vector2d v2 = transformed_decimated_points[p.first];
//            Eigen::Vector2d v1 = decimated_points[p.first];
//            Eigen::Vector2d v2 = transformed_decimated_points[p.second];
//            error += (v1 - v2).norm();
//        }
//        error /= idxsSet.size();
//        cerr << "Error = " << error << endl;
//        cerr << "#Correspondences = " << idxsSet.size() << endl;
//        waitKey(500);
//    }
////    transformed_points.clear();
////    transformed_points.resize(reference_points.size());
////    copy(reference_points.begin(), reference_points.end(), transformed_points.begin());
////    transformed_decimated_points.clear();
//    cerr << "Press Q to quit!\n";
//    key = 0;
//    while(tolower(key)!='q') key = waitKey(0);
    destroyWindow("SOLVER Demo");
//    }

    logFile.close();
    return 0;
}

int completeTest(ifstream& logFile, const double& density){
    int height = 540;
    int width = 960;
    RGBImage image;
    image.create(height, width);
    image = 0;
    RGBImage complete_map;
    complete_map.create(height, width);
    complete_map = cv::Vec3b(127, 127, 127);
    IndexImage idxsImage;
    idxsImage.create(height, width);
    idxsImage = -1;

    Vector2dVector reference_points;
    Vector2dVector points;
    Vector2dVector visible_points;
    Vector2dVector decimated_reference_points;
    Vector2dVector decimated_points;
//    Eigen::Vector2d robot_position;
    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
    T.translation() = Eigen::Vector2d((width>>1)*density, (height>>1)*density);

    IndexSet idxsSet;

    DistanceMap map = DistanceMap();
    ransac r = ransac();

    char key=0;
    double max_distance = 8.;
//    robot_position.setZero(2);
//    robot_position.x() = 320;
//    robot_position.y() = 240;

//    robot_position = T*robot_position;

    ExtPointsImage pointsImage;
    ExtPointsImage transformed_pointsImage;
    pointsImage.create(height, width);
    transformed_pointsImage.create(height, width);

    namedWindow("Complete Test");
    namedWindow("Complete Map");

    readCarmenData(logFile, string(" "), reference_points);
    if(!logFile.eof()) readCarmenData(logFile, string(" "), points);
    else {
        cerr << "Just one scan read. Please check input file!\n";
        return 0;
    }

    toCameraFrame(reference_points, visible_points, height, width, density);
//    decimatePoints(visible_points, idxsSet, decimated_reference_points, density);
    decimatePoints(pointsImage, visible_points, idxsSet, decimated_reference_points, density);

    reference_points.clear();
    toWorldFrame(decimated_reference_points, reference_points, height, width, density);
//    drawMap(complete_map, reference_points, robot_position, density);
//    imshow("Complete Map", complete_map);
//    waitKey(10);

    int num_correspondences = 5;
//    waitKey(0);
    do{
        visible_points.clear();
        idxsSet.clear();
        toCameraFrame(points, visible_points, height, width, density);
//        decimatePoints(visible_points, idxsSet, decimated_points, density);
        decimatePoints(transformed_pointsImage, visible_points, idxsSet, decimated_points, density);
        fillIndexes(idxsImage, decimated_points, density);
        map.compute(idxsImage, density, max_distance);
        idxsSet.clear();
//        map.correspondences(idxsImage, decimated_reference_points, density, idxsSet);
        map.correspondences_withNormals(idxsImage, decimated_reference_points, decimated_points, density, idxsSet);
        cerr << "#Correspondences = " << idxsSet.size() << endl;
        double error = decimated_points.size();
        Eigen::Isometry2d H;
        while(error>0.25){
//            cerr << "ENTERING\n";
            H.setIdentity();
            image = map.getImage(max_distance);
            drawPoints(image, decimated_points, density, cv::Scalar(0, 0, 255));
            drawPoints(image, decimated_reference_points, density, cv::Scalar(255, 0, 0));
            drawCorrespondences(image, idxsSet, decimated_points, decimated_reference_points, density);
            imshow("Complete Test", image);
            key = waitKey(10);
            reference_points.clear();
            points.clear();
            toWorldFrame(decimated_reference_points, reference_points, height, width, density);
            toWorldFrame(decimated_points, points, height, width, density);
            r.compute(reference_points,points,idxsSet,num_correspondences, 1, density);
            H = r.best_H;
            applyTransformation(H, reference_points);
            decimated_reference_points.clear();
            decimated_points.clear();
            toCameraFrame(reference_points, decimated_reference_points, height, width, density);
            toCameraFrame(points, decimated_points, height, width, density);
            idxsSet.clear();
//            map.correspondences(idxsImage, decimated_reference_points, density, idxsSet);
            map.correspondences_withNormals(idxsImage, decimated_reference_points, decimated_points, density, idxsSet);
            image = map.getImage(max_distance);
            drawPoints(image, decimated_reference_points, density, cv::Scalar(255, 0, 0));
            drawPoints(image, decimated_points, density, cv::Scalar(0, 0, 255));
            drawCorrespondences(image, idxsSet, decimated_points, decimated_reference_points, density);
            imshow("Complete Test", image);
            key = waitKey(10);
            error = 0.;
            for(IndexSetIterator it=idxsSet.begin(); it!=idxsSet.end(); ++it){
                IndexPair p = *it;
                Eigen::Vector2d v1 = decimated_reference_points[p.first];
                Eigen::Vector2d v2 = decimated_points[p.second];
                error += (v1 - v2).norm();
            }
            error /= idxsSet.size();
            cerr << "Error = " << error << endl;
            cerr << "#Correspondences = " << idxsSet.size() << endl;
//            cerr << "EXITING";
        }
//        cerr << "Previous Robot Pos. = \n" << robot_position << endl;
//        robot_position = H*robot_position;
//        cerr << "Robot Pos. = \n" << robot_position << endl;
//        robot_position_camera = T*(robot_position.array()/density).array().round();
//        cerr << "Robot Pos. Camera = \n" << robot_position_camera << endl;
//        drawMap(complete_map, points, robot_position, density);
//        T = T*H;
        drawMap(complete_map, points, T, (H*T).inverse(), density);
        imshow("Complete Map", complete_map);
//        robot_position = T.inverse()*robot_position;
//        robot_position *= density;
//        robot_position = T*robot_position;
        key = waitKey(10);
        decimated_reference_points.clear();
        decimated_reference_points.resize(decimated_points.size());
        std::copy(decimated_points.begin(), decimated_points.end(), decimated_reference_points.begin());
        points.clear();
        decimated_points.clear();
        pointsImage.create(height, width);
        transformed_pointsImage.create(height, width);
        readCarmenData(logFile, string(" "), points);
    }while(!logFile.eof());
    cerr << "Press Q to quit!\n";
    key = 0;
    while(tolower(key)!='q') key = waitKey(0);
    destroyWindow("Complete Test");
//    }

    logFile.close();
    return 0;
}

int main(int argc, char** argv)
{
    if(argc<2)
    {
        cout << "[USAGE]: ./SM_DistanceMap file_to_read pixel_size_in_meters" << endl;
        return -1;
    }
    float density = atof(argv[2]);
    char key=0;
    const char ESC_key=27;

    RGBImage Help;
    Help.create(480, 640);
    Help = 0;
    putText(Help, "K - Kabsch Demo", cv::Point(10, 30), FONT_HERSHEY_SIMPLEX, 1., cv::Scalar(0, 255, 0));
    putText(Help, "M - Distance Map Demo", cv::Point(10, 60), FONT_HERSHEY_SIMPLEX, 1., cv::Scalar(0, 255, 0));
    putText(Help, "R - RANSAC 1Shot Demo", cv::Point(10, 90), FONT_HERSHEY_SIMPLEX, 1., cv::Scalar(0, 255, 0));
    putText(Help, "S - Solver Demo", cv::Point(10, 120), FONT_HERSHEY_SIMPLEX, 1., cv::Scalar(0, 255, 0));
    putText(Help, "C - Complete Test", cv::Point(10, 150), FONT_HERSHEY_SIMPLEX, 1., cv::Scalar(0, 255, 0));
    putText(Help, "ESC - Quit program", cv::Point(10, 180), FONT_HERSHEY_SIMPLEX, 1., cv::Scalar(0, 255, 0));

    namedWindow("Catch keys");
    imshow("Catch keys", Help);

    while(key!=ESC_key){
        switch(toupper(key)){
        case 'C': {
            ifstream carmenLog(argv[1]);
            if(carmenLog.fail()){
                cerr << "Error opening the file!\n";
                return -1;
            }
            completeTest(carmenLog, density);
            break;
        }
        case 'K': DLT2Ddemo(density);break;
        case 'M': {
            ifstream carmenLog(argv[1]);
            if(carmenLog.fail()){
                cerr << "Error opening the file!\n";
                return -1;
            }
            DMdemo(carmenLog, density);
            break;
        }
        case 'R': {
            ifstream carmenLog(argv[1]);
            if(carmenLog.fail()){
                cerr << "Error opening the file!\n";
                return -1;
            }
            RANSACdemo(carmenLog, density);
            break;
        }
        case 'S': {
            ifstream carmenLog(argv[1]);
            if(carmenLog.fail()){
                cerr << "Error opening the file!\n";
                return -1;
            }
            SOLVERdemo(carmenLog, density);
            break;
        }
        default: break;
        }
        cerr << "Chose an option!\n";
        key = waitKey(0);
    }

    return 0;
}
