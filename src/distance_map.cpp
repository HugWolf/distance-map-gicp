#include "distance_map.h"

using namespace std;

int DistanceMap::neighborhood(MapCell *neighbors, const int& row, const int& col, const int &rows, const int &cols){
    //cerr << "(" << row << "," << col << ")\n";
    int min_row = row-1<0 ? 0:row-1;
    int max_row = row+1>rows-1 ? rows-1:row+1;
    int min_col = col-1<0 ? 0:col-1;
    int max_col = col+1>cols-1 ? cols-1:col+1;
    int k = 0;
    //cerr << "min_row = " << min_row << "; min_col = " << min_col << "\n";
    //cerr << "max_row = " << max_row << "; max_col = " << max_col << "\n";
    for(int row_range=min_row; row_range<=max_row; ++row_range){
        for(int col_range=min_col; col_range<=max_col; ++col_range){
            if(row_range!=row || col_range!=col){
                neighbors[k] = _distance_map(row_range, col_range);
                //cerr << "(" << neighbors[k].row << "," << neighbors[k].col << "," << neighbors[k].distance << "," << neighbors[k].parent_row << "," << neighbors[k].parent_col << ") | ";
                ++k;
            }
        }
    }
    //cerr << "\n";
    return k;
}

void DistanceMap::compute(IndexImage& iImage, const float& density, const float& max_distance){
    _distance_map.resize(iImage.rows, iImage.cols);
    MapQueue q;

    int max_pixel_distance = std::round((max_distance*max_distance)/density);
    for(int i=0; i<iImage.rows; ++i){
        for(int j=0; j<iImage.cols; ++j){
            MapCell c = MapCell();
            c.row = i;
            c.col = j;
            int idx = iImage.at<int>(i, j);
            if(idx>-1){
                c.parent_row = c.row;
                c.parent_col = c.col;
                c.distance = 0;
                q.push(c);
            }
            _distance_map(i, j) = c;
        }
    }

    MapCell neighbors[8];
    int k = 0;
    while(!q.empty()){
        MapCell current_node = q.top();
        int parentIdx = iImage.at<int>(current_node.parent_row, current_node.parent_col);
        q.pop();
        k = neighborhood(neighbors, current_node.row, current_node.col, iImage.rows, iImage.cols);
        for(int i=0; i<k; ++i){
            MapCell neighbor = neighbors[i];
            int n_row = neighbor.row;
            int n_col = neighbor.col;
            int row_distance = n_row - current_node.parent_row;
            int col_distance = n_col - current_node.parent_col;
            int d = ((row_distance*row_distance) + (col_distance*col_distance));
            if(d<max_pixel_distance && d<neighbor.distance){
//                cerr << "[BEFORE] neighbor = (" << neighbor.row << "," << neighbor.col << "," << neighbor.distance << "," << neighbor.parent_row << "," << neighbor.parent_col << ") --- ";
                neighbor.parent_row = current_node.parent_row;
                neighbor.parent_col = current_node.parent_col;
                neighbor.distance = d;
                _distance_map(n_row, n_col) = neighbor;
//                cerr << "[AFTER] neighbor = (" << neighbor.row << "," << neighbor.col << "," << neighbor.distance << "," << neighbor.parent_row << "," << neighbor.parent_col << ") --- ";
//                cerr << "[DM] neighbor = (" << _distance_map(n_row, n_col).row << "," << _distance_map(n_row, n_col).col << "," << _distance_map(n_row, n_col).distance << "," << _distance_map(n_row, n_col).parent_row << "," << _distance_map(n_row, n_col).parent_col << ")\n";
                iImage.at<int>(n_row, n_col) = parentIdx;
                q.push(neighbor);
            }
        }
    }

    _distances_image.create(iImage.rows, iImage.cols);
    for(int i=0; i<_distances_image.rows; ++i){
        float* dI_row = _distances_image.ptr<float>(i);
        for(int j=0; j<_distances_image.cols; ++j){
            MapCell c = _distance_map(i,j);
            dI_row[j] = std::sqrt(c.distance)*density;
        }
    }
}

RGBImage DistanceMap::getImage(const float& max_distance){
    RGBImage image;
    image.create(_distances_image.rows, _distances_image.cols);
    image = cv::Vec3b(0, 0, 0);
    for (int r=0; r<_distances_image.rows; r++){
        for (int c=0; c<_distances_image.cols; c++){
            float dist=_distances_image.at<float>(r,c);
            if (dist>max_distance) continue;
            int weight = 127*(dist/max_distance);
            int v = (dist==0) ? 255: 127-weight;
            image.at<cv::Vec3b>(r,c) = cv::Vec3b(v,v,v);
        }
    }
    return image;
}

void DistanceMap::correspondences(const IndexImage& indexes, const Vector2dVector& points, const float& density, IndexSet& correspondences_set){
    int rows = indexes.rows;
    int cols = indexes.cols;
    if(correspondences_set.size()>0) correspondences_set.clear();
    for(size_t i=0; i<points.size(); ++i){
        Eigen::Vector2d point = points[i];
        int x = round(point.x()/density);
        int y = round(point.y()/density);
        if(x<0 || x>=cols) continue;
        if(y<0 || y>=rows) continue;
        int idx = indexes.at<int>(y, x);
        if(idx>-1) correspondences_set.insert(make_pair(i, idx));
    }
}

void DistanceMap::pointNormal(const Eigen::Vector2d point, const Vector2dVector &points, Eigen::Vector2d &normal_vector, const int &neighbours){
    Eigen::Matrix2d covariance;
    Eigen::MatrixXd Neighbourhood;
    Eigen::MatrixXd V;
    NeighbourDistance N;

    normal_vector.setZero(2);
    covariance.setZero(2,2);
    Neighbourhood.setZero(2, neighbours);
    V.setZero(2,2);

    for(size_t i=0; i<points.size(); ++i){
        Eigen::Vector2d v = points[i];
        double error = (v-point).norm();
        NeighbourEntry ngh = NeighbourEntry();
        ngh.index = i;
        ngh.error = error;
        N.push(ngh);
    }

    // Remove the first entry, which is the point itself
    N.pop();

    for(int i=0; i<neighbours; ++i){
        NeighbourEntry ngh = N.top();
        N.pop();
        Neighbourhood.col(i) = points[ngh.index];
    }

    Eigen::MatrixXd Neighbourhood_centered = Neighbourhood.colwise() - Neighbourhood.rowwise().mean();
    covariance = (Neighbourhood_centered*Neighbourhood_centered.transpose())*(1.0/(neighbours));
    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigSolv;
    eigSolv.compute(covariance, Eigen::ComputeEigenvectors);
    if (eigSolv.info() != Eigen::Success) abort();
    V = eigSolv.eigenvectors();
    normal_vector = V.col(0);

}

void DistanceMap::correspondences_withNormals(const IndexImage& indexes, const Vector2dVector& reference_points, const Vector2dVector& points, const float& density, IndexSet& correspondences_set){
    Eigen::Vector2d refereces_normal;
    Eigen::Vector2d points_normal;
    int rows = indexes.rows;
    int cols = indexes.cols;
    if(correspondences_set.size()>0) correspondences_set.clear();
    for(size_t i=0; i<reference_points.size(); ++i){
        Eigen::Vector2d reference_point = reference_points[i];
        int x = round(reference_point.x()/density);
        int y = round(reference_point.y()/density);
        if(x<0 || x>=cols) continue;
        if(y<0 || y>=rows) continue;
        int idx = indexes.at<int>(y, x);
        if(idx>-1) {
            refereces_normal.setZero(2);
            points_normal.setZero(2);
            Eigen::Vector2d point = points[idx];
            pointNormal(point, points, points_normal, 20);
            pointNormal(reference_point, reference_points, refereces_normal, 20);
            if((points_normal-refereces_normal).norm()<=0.27) correspondences_set.insert(make_pair(i, idx));
        }
    }
}
