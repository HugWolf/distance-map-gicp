#include "drawing_utils.h"

void drawPoints(RGBImage& dImage, const Vector2dVector& points, const float& density, cv::Scalar color){
    for(size_t i=0; i<points.size(); ++i){
        Eigen::Vector2d point = points[i];
        int x = round(point.x()/density);
        int y = round(point.y()/density);
        cv::circle(dImage, cv::Point(x, y), 1, color, 1);
    }
}

void drawSelection(RGBImage& dImage, const Vector2dVector& points, const size_t& idx, const float& density, cv::Scalar color){
    Eigen::Vector2d point = points[idx];
    int x = round(point.x()/density);
    int y = round(point.y()/density);
    cv::circle(dImage, cv::Point(x, y), 5, color, 1);
}

void drawCorrespondences(RGBImage& image, const IndexSet& indexes, const Vector2dVector& references, const Vector2dVector& points, const float& density){
    for(IndexSetIterator it=indexes.begin(); it!=indexes.end(); ++it){
        IndexPair p = *it;
        int point_idx = p.first;
        int reference_idx = p.second;
//        std::cerr << "<" << reference_idx << ", " << point_idx << ">\n";
        Eigen::Vector2d point = points[point_idx];
        Eigen::Vector2d reference = references[reference_idx];
//        std::cerr << "Reference = \n" << reference << std::endl;
//        std::cerr << "Point = \n" << point << std::endl;
        int point_x = round(point.x()/density);
        int point_y = round(point.y()/density);
        int reference_x = round(reference.x()/density);
        int reference_y = round(reference.y()/density);
        cv::line(image, cv::Point(point_x, point_y), cv::Point(reference_x, reference_y), cv::Scalar(0, 255, 0));
    }
//    std::cerr << std::endl;
}

void drawMap(RGBImage& image, const Vector2dVector& points, const Eigen::Vector2d &robot_position, const float& density){
    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
//    T.translation() = robot_position*density;
    Eigen::Vector3d RP(robot_position(0), -robot_position(1), 1.);
    Eigen::Vector2d robot_position_camera(0., 0.);
    T = v2T(RP);
    T = T.inverse();
    robot_position_camera = T*robot_position;
    for(size_t i=0; i<points.size(); ++i){
        Eigen::Vector2d point = points[i];
        point = T*point;
        int x = round(point.x()/density);
        int y = round(point.y()/density);
//        cv::circle(image, cv::Point(point.x(), point.y()), 1, cv::Vec3b(0, 0, 0), -1);
//        cv::line(image, cv::Point(robot_position.x(), robot_position.y()), cv::Point(point.x(), point.y()), cv::Scalar(255, 255, 255));
        cv::circle(image, cv::Point(x, y), 1, cv::Vec3b(0, 0, 0), -1);
        cv::line(image, cv::Point(robot_position_camera.x()/density, robot_position_camera.y()/density), cv::Point(x, y), cv::Scalar(255, 255, 255));
    }
    cv::circle(image, cv::Point(robot_position_camera.x()/density, robot_position_camera.y()/density), 1, cv::Vec3b(0, 255, 0), -1);
}

void drawMap(RGBImage& image, const Vector2dVector& points, const Eigen::Isometry2d& T, const Eigen::Isometry2d& H, const float& density){
    Eigen::Vector2d robot_position(0., 0.);
    robot_position = T*robot_position;
    for(size_t i=0; i<points.size(); ++i){
        Eigen::Vector2d point = points[i];
        point = H*point;
        int x = round(point.x()/density);
        int y = round(point.y()/density);
//        cv::circle(image, cv::Point(point.x(), point.y()), 1, cv::Vec3b(0, 0, 0), -1);
//        cv::line(image, cv::Point(robot_position.x(), robot_position.y()), cv::Point(point.x(), point.y()), cv::Scalar(255, 255, 255));
        cv::circle(image, cv::Point(x, y), 3, cv::Vec3b(0, 0, 0), -3);
        cv::line(image, cv::Point(robot_position.x()/density, robot_position.y()/density), cv::Point(x, y), cv::Scalar(255, 255, 255));
    }
    cv::circle(image, cv::Point(robot_position.x()/density, robot_position.y()/density), 3, cv::Vec3b(0, 255, 0), -1);
}
