#include "processing_utils.h"

#define HALF_DEGREE (0.0087266462599716478)

using namespace std;
using namespace cv;

//void toWorldFrame(const Vector2dVector& points, Vector2dVector& transformed, const int &rows, const int &cols, const float &density){
//    transformed.resize(points.size());
//    Eigen::Isometry2d T = v2T(Eigen::Vector3d((cols>>1)*density, (rows>>1)*density, 0.));
//    for(size_t i=0; i<points.size(); i++){
//        Eigen::Vector2d v = points[i];
//        transformed[i] = T*v;
//    }
//}

void toCameraFrame(const Vector2dVector& points, Vector2dVector& transformed, const int &rows, const int &cols, const float &density){
//    transformed.resize(points.size());
    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
    T.translation() = Eigen::Vector2d((cols>>1)*density, (rows>>1)*density);
    for(size_t i=0; i<points.size(); i++){
        Eigen::Vector2d v = T*points[i];
        if(v.x()<0 || v.x()>cols-1) continue;
        if(v.y()<0 || v.y()>rows-1) continue;
        transformed.push_back(v);
    }
    std::cerr << "Visible points: " << transformed.size() << "/" << points.size() << std::endl;
}

void toWorldFrame(const Vector2dVector& points, Vector2dVector& transformed, const int &rows, const int &cols, const float &density){
//    transformed.resize(points.size());
    Eigen::Isometry2d T(Eigen::Isometry2d::Identity());
    T.translation() = Eigen::Vector2d((cols>>1)*density, (rows>>1)*density);
    for(size_t i=0; i<points.size(); i++){
        Eigen::Vector2d v = T.inverse()*points[i];
        transformed.push_back(v);
    }
}

void decimatePoints(PointsImage& pImage, IndexImage& iImage, Vector2dVector &reference_points, IndexSet& indexes, Vector2dVector& decimated_points, const float& density){
    int rows = pImage.rows;
    int cols = pImage.cols;
//    Vector2dVector transformed_points;
//    toWorldFrame(reference_points, transformed_points, rows, cols, density);
    for(size_t i=0; i<reference_points.size(); ++i)
    {
        Eigen::Vector2d v = reference_points[i];
        int x = round(v.x()/density);
        int y = round(v.y()/density);
        x = (x<0) ? 0:x;
        x = (x>cols) ? cols-1:x;
        y = (y<0) ? 0:y;
        y = (y>rows) ? rows-1:y;
        Point3d p = pImage.at<Point3d>(y, x);
        p += Point3d(v.x(), v.y(), 1);
        pImage.at<Point3d>(y, x) = p;
        indexes.insert(make_pair(y, x));
    }
    cout << "Points population decimated from " << reference_points.size() << " points to " << indexes.size() << " points.\n";
    for(IndexSetIterator it=indexes.begin(); it!=indexes.end(); ++it)
    {
        IndexPair pair = *it;
        Point3d p = pImage.at<Point3d>(pair.first, pair.second);
        p.x /= p.z;
        p.y /= p.z;
        pImage.at<Point3d>(pair.first, pair.second) = p;
        decimated_points.push_back(Eigen::Vector2d(p.x, p.y));
    }
    for(size_t i=0; i<decimated_points.size(); ++i)
    {
        Eigen::Vector2d p = decimated_points[i];
        int x = round(p.x()/density);
        int y = round(p.y()/density);
        x = (x<0) ? 0:x;
        x = (x>cols) ? cols:x;
        y = (y<0) ? 0:y;
        y = (y>rows) ? rows:y;
        iImage.at<int>(y, x) = 127*i;
    }
}

void decimatePoints(ExtPointsImage& pImage, Vector2dVector &reference_points, IndexSet& indexes, Vector2dVector& decimated_points, const float& density){
    int rows = pImage.rows;
    int cols = pImage.cols;
//    Vector2dVector transformed_points;
    if(indexes.size()>0) indexes.clear();
//    toWorldFrame(reference_points, transformed_points, rows, cols, density);
//    for(size_t i=0; i<transformed_points.size(); ++i)
    for(size_t i=0; i<reference_points.size(); ++i)
    {
//        Eigen::Vector2d v = transformed_points[i];
        Eigen::Vector2d v = reference_points[i];
        int x = round(v.x()/density);
        int y = round(v.y()/density);
//        x = (x<0) ? 0:x;
//        x = (x>cols) ? cols-1:x;
//        y = (y<0) ? 0:y;
//        y = (y>rows) ? rows-1:y;
        if(x<0 || x>=cols) continue;
        if(y<0 || y>=rows) continue;
        ExtPoint p = pImage.at<ExtPoint>(y, x);
        p.storage_point += v;
        p.n_items += 1;
        pImage.at<ExtPoint>(y, x) = p;
        indexes.insert(make_pair(y, x));
    }
//    cout << "Points population decimated from " << transformed_points.size() << " points to " << indexes.size() << " points.\n";
    cout << "Points population decimated from " << reference_points.size() << " points to " << indexes.size() << " points.\n";
    for(IndexSetIterator it=indexes.begin(); it!=indexes.end(); ++it)
    {
        IndexPair pair = *it;
        ExtPoint p = pImage.at<ExtPoint>(pair.first, pair.second);
        p.centroid = p.storage_point/p.n_items;
        pImage.at<ExtPoint>(pair.first, pair.second) = p;
        decimated_points.push_back(p.centroid);
    }
}

void decimatePoints(Vector2dVector &reference_points, IndexSet& indexes, Vector2dVector& decimated_points, const float& resolution){
    if(indexes.size()>0) indexes.clear();
    Eigen::Vector2d previous_point(0., 0.);
    for(size_t i=0; i<reference_points.size(); ++i)
    {
        Eigen::Vector2d v = reference_points[i];
        double norm = (v-previous_point).norm();
        if(norm>=resolution) {
            int x = round(v.x()/resolution);
            int y = round(v.y()/resolution);
            indexes.insert(make_pair(y, x));
            decimated_points.push_back(v);
        }
        previous_point = v;
    }
    cout << "Points population decimated from " << reference_points.size() << " points to " << indexes.size() << " points.\n";
}

int readCarmenData(ifstream& file, const string delimiter, Vector2dVector& readings){
    if(!file.is_open()) return 0;
    if(file.eof()) return 0;
    string line;
    size_t position;
    float theta = 0;
    float reading = 0;
    std::getline(file,line);
    cout << "Reading from input file";
    while((position=line.find(delimiter))!=string::npos)
    {
        cout << ".";
        sscanf(line.data(),"%f %*s",&reading);
        double r_x = 0;
        double r_y = 0;
        polarToEucledian(reading,theta,r_x,r_y);
        readings.push_back(Eigen::Vector2d(r_x, r_y));
        line.erase(0,position+delimiter.length());
        theta += HALF_DEGREE;
    }
    cout << readings.size() << " records read!" << endl;
    return 1;
}

int init(){return 0;}

void applyTransformation(const Eigen::Isometry2d& T, Vector2dVector& points){
    for(size_t i=0; i<points.size(); ++i){
        points[i] = T*points[i];
    }
}

void applyTransformation(const Eigen::Isometry2d& T, const Vector2dVector& points, Vector2dVector& transformed_points){
    for(size_t i=0; i<points.size(); ++i){
        transformed_points.push_back(T*points[i]);
    }
}

void fillIndexes(IndexImage& iImage, const Vector2dVector& reference_points, const float& density){
    iImage = -1;
    int rows = iImage.rows;
    int cols = iImage.cols;
    for(size_t i=0; i<reference_points.size(); ++i)
    {
        Eigen::Vector2d p = reference_points[i];
        int x = round(p.x()/density);
        int y = round(p.y()/density);
        x = (x<0) ? 0:x;
        x = (x>cols) ? cols-1:x;
        y = (y<0) ? 0:y;
        y = (y>rows) ? rows-1:y;
        iImage.at<int>(y, x) = i;
    }
}
