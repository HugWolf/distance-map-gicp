#include "ransac.h"

using namespace Eigen;

void ransac::compute(const Vector2dVector& references, const Vector2dVector& points, const IndexSet& correspondences_set, const int& n_point_correspondences, const int& use_normals, const double& threshold, const double& probability){
    CorrsVector indexes(correspondences_set.size());
    std::copy(correspondences_set.begin(), correspondences_set.end(), indexes.begin());
    std::set<int> random_corrs_idxs;
    std::pair<std::set<int>::iterator, bool> check;
//    solver S = solver();

    int col;
    int inliers;
    double log_1mp = log(1-probability);
    double eps = pow(2.0, -52.0);
    DLT2D_Mat M1;
    DLT2D_Mat M2;
    Vector3d l1;
    Vector3d l2;
    if(use_normals){
        M1.setZero(3, n_point_correspondences + (n_point_correspondences%2));
        M2.setZero(3, n_point_correspondences + (n_point_correspondences%2));
    }
    else {
        M1.setZero(3, n_point_correspondences);
        M2.setZero(3, n_point_correspondences);
    }
    l1.setZero(3);
    l2.setZero(3);

    int N_samples = correspondences_set.size()<<1;
    int sample_count = 0;
    distribution = std::uniform_int_distribution<int>(0, int(indexes.size()-1));
    while (N_samples>sample_count)
    {
        //random sampling n correspondences (2n points)
        col = 0;
        inliers = 0;
        int idx;
        do{
            idx = distribution(generator);
            check = random_corrs_idxs.insert(idx);
            if(check.second){
                IndexPair correspondence = indexes[idx];
                if(use_normals){
                    Vector3d v1(0., 0., 0.);
                    v1.head(2) = references[correspondence.first];
                    v1(2) = 1.;
                    Vector3d v2(0., 0., 0.);
                    v2.head(2) = points[correspondence.second];
                    v2(2) = 1.;
                    Vector2d reference_normal(0., 0.);
                    Vector2d point_normal(0., 0.);
                    S.pointNormal(references[correspondence.first], references, reference_normal, 20);
                    S.pointNormal(points[correspondence.second], points, point_normal, 20);
                    M1.col(col) << v1;
                    M1.col(col+1) << reference_normal.x(), reference_normal.y(), 1.;
                    M2.col(col) << v2;
                    M2.col(col+1) << point_normal.x(), point_normal.y(), 1.;
                    if(col==0){
                        l1 = initLine(v1, Eigen::Vector3d(reference_normal.x(), reference_normal.y(), 1.));
                        l2 = initLine(v2, Eigen::Vector3d(point_normal.x(), point_normal.y(), 1.));
                    }
                    if(col>0){
                        bool colinear1 = checkColinearity(v1, l1);
                        bool colinear2 = checkColinearity(v2, l2);
                        if(colinear1 || colinear2){
                            random_corrs_idxs.erase(check.first);
                            col -= 2;
                        }
                    }
                    col += 2;
                }
                else{
                    Vector3d v1(0., 0., 0.);
    //                v1.head(2) = references[correspondence.second];
                    v1.head(2) = references[correspondence.first];
                    v1(2) = 1.;
                    Vector3d v2(0., 0., 0.);
    //                v2.head(2) = points[correspondence.first];
                    v2.head(2) = points[correspondence.second];
                    v2(2) = 1.;
                    M1.col(col) << v1;
                    M2.col(col) << v2;
                    if(col==1){
                        l1 = initLine(M1.col(col-1), v1);
                        l2 = initLine(M2.col(col-1), v2);
                    }
                    if(col>1){
                        bool colinear1 = checkColinearity(v1, l1);
                        bool colinear2 = checkColinearity(v2, l2);
                        if(colinear1 || colinear2){
                            random_corrs_idxs.erase(check.first);
                            --col;
                        }
                    }
                    ++col;
                }
            }
        }while(M1.rightCols(1).isZero(1e-17));
        DLT2D_Transform2d H_tilde;
//        DLT2D(M1, M2, n_point_correspondences, H_tilde);
        Kabsch(M1, M2, H_tilde);
//        std::cerr << "[DLT] H = \n" << H_tilde.matrix() << std::endl;
        for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it){
            IndexPair p = *it;
//            Vector2d v1 = references[p.second];
//            Vector2d v2 = points[p.first];
            Vector2d v1 = references[p.first];
            Vector2d v2 = points[p.second];
            double err = symmTransfError(v1, v2, H_tilde);
//            std::cerr << "error = " << err << "...";
            if (err<threshold) ++inliers;
        }
//        std::cerr << std::endl;
        HomographyEntry ent = HomographyEntry();
        ent.inliers = inliers;
        std::copy(H_tilde.data(),H_tilde.data()+9,ent.h.data());
        q.push(ent);
        double fracInliers = inliers/(double)correspondences_set.size();
        double percOutliers = 1 - pow(fracInliers, n_point_correspondences<<1);
        ent.std_dev = sqrt(percOutliers)/pow(fracInliers, n_point_correspondences<<1);
        percOutliers = std::max(eps, percOutliers);
        percOutliers = std::min(1-eps, percOutliers);
//        std::cerr << "Inliers = " << inliers << std::endl;
//        std::cerr << "%Outliers = " << percOutliers << std::endl;
        //compute new N_samples
        N_samples = log_1mp/log(percOutliers);
        ++sample_count;
        if(use_normals){
            M1.setZero(3, n_point_correspondences + (n_point_correspondences%2));
            M2.setZero(3, n_point_correspondences + (n_point_correspondences%2));
        }
        else {
            M1.setZero(3, n_point_correspondences);
            M2.setZero(3, n_point_correspondences);
        }
        l1.setZero(3);
        l2.setZero(3);
        random_corrs_idxs.clear();
    }
//    std::cerr << "Size of queue = " << q.size() << std::endl;
    HomographyEntry bestH = q.top();
//    q.pop();
//    std::cerr << "Inliers = " << bestH.inliers << std::endl;
    best_H = Isometry2d::Identity();
    if (bestH.inliers>=(correspondences_set.size()>>1)) std::copy(bestH.h.data(), bestH.h.data()+9, best_H.data());
//    std::cerr << "H = \n" << best_H.matrix() << std::endl;
//    S.standardMatchLM(references, points, correspondences_set, best_H, threshold, 0.0005, 20);
    if(use_normals) S.generalizedMatchLM(references, points, correspondences_set, best_H, 0.0005, 20);
    else S.standardMatchLM(references, points, correspondences_set, best_H, 0.0005, 20);
    q = HomographyQueue();
}

void ransac::DLT2D(const DLT2D_Mat &src, const DLT2D_Mat &dst, const int& n_point_correspondences, DLT2D_Transform2d& H){
    //scale the 1st point cloud so that avg distance from (0, 0) is sqrt(2)
    DLT2D_Transform2d T;
    scalePoints(src, T, 0);
    DLT2D_Mat src_scaled = T*src;
    //scale the 2nd point cloud so that avg distance from (0, 0) is sqrt(2)
    DLT2D_Transform2d T_prime;
    scalePoints(dst, T_prime, 0);
    DLT2D_Mat dst_scaled = T_prime*dst;
    //construct A 2nx9
    MatrixXd A;
    arrangeEqsSet(src_scaled, dst_scaled, n_point_correspondences, A);
    //SVD
    JacobiSVD<MatrixXd> svd(A, ComputeFullV | FullPivHouseholderQRPreconditioner);
    VectorXd V_lastCol = svd.matrixV().col(svd.matrixV().cols()-1);
    //normalize such that h9 = V(9) = 1
    V_lastCol /= V_lastCol.tail(1)(0);
    DLT2D_Transform2d H_tilde(DLT2D_Transform2d::Identity());
    std::copy(V_lastCol.data(), V_lastCol.data()+9, H_tilde.data());
    H_tilde.matrix().transposeInPlace();
    //normalize the rotation angle
    double angle = std::atan2(H_tilde(1,0), H_tilde(0,0));
    angle = angle - (std::floor(angle/360)*360);
    //H = T'^{-1}*H*T
    H = T_prime.inverse()*H_tilde*T;
}

void ransac::Kabsch(const DLT2D_Mat &src, const DLT2D_Mat &dst, DLT2D_Transform2d& H){
    MatrixXd src_scaled;
    MatrixXd dst_scaled;
    Vector2d src_centroid;
    Vector2d dst_centroid;
    //translate the 1st point cloud's centroid is in (0, 0)
    scalePointsKabsch(src, src_scaled, src_centroid);
    //translate the 2nd point cloud's centroid in (0, 0)
    scalePointsKabsch(dst, dst_scaled, dst_centroid);
    //construct A
    MatrixXd A;
    A = src_scaled*(dst_scaled.transpose());
    //SVD
    JacobiSVD<MatrixXd> svd(A, ComputeFullU | ComputeFullV | FullPivHouseholderQRPreconditioner);
    double det = (svd.matrixV()*svd.matrixU().transpose()).determinant();
    det = (det>0) ? det:(-1*det);
    Matrix2d I = Matrix2d::Identity(2,2);
    I(1,1) = det;
    Matrix2d R = svd.matrixV()*I*svd.matrixU().transpose();
    H = DLT2D_Transform2d::Identity();
    H.linear() = R;
    H.translation() = dst_centroid - (R*src_centroid);
}

void ransac::scalePoints(const DLT2D_Mat &points, DLT2D_Transform2d &T, const int& searchIsometry){
    Vector3d centroid;
    centroid.setZero(3);
    centroid(2) = 1.0;
    centroid.head(2) = points.block(0, 0, 2, points.cols()).rowwise().mean();
    double scale_factor;
    scale_factor = (searchIsometry) ? 1.:M_SQRT2 / ((points.colwise() - centroid).colwise().norm()).mean();
    T = DLT2D_Transform2d::Identity();
    T.translation() = (-1)*centroid.head(2);
    T *= Scaling(scale_factor);
}

void ransac::scalePointsKabsch(const DLT2D_Mat &points, MatrixXd& scaled_points, Vector2d& centroid){
    centroid.setZero(2);
    scaled_points.setZero(2, points.cols());
    centroid = points.block(0, 0, 2, points.cols()).rowwise().mean();
    scaled_points = points.block(0, 0, 2, points.cols()).colwise() - centroid;
}

void ransac::arrangeEqsSet(const DLT2D_Mat &reference, const DLT2D_Mat &points, const int& n_point_correspondences, MatrixXd& A){
    A.setZero(2*n_point_correspondences, 9);
    int j = 0;
    for(int i=0; i<reference.cols(); i++)
    {
        j = i<<1;
        Vector3d reference_point = reference.col(i);
        Vector3d point = points.col(i);
        A.row(j)   << 0.                         , 0.                         , 0.                         , ((-1)*point(2))*reference_point(0), ((-1)*point(2))*reference_point(1), ((-1)*point(2))*reference_point(2), point(1)*reference_point(0)       , point(1)*reference_point(1)       , point(1)*reference_point(2);
        A.row(j+1) << point(2)*reference_point(0), point(2)*reference_point(1), point(2)*reference_point(2), 0.                                , 0.                                , 0.                                , ((-1)*point(0))*reference_point(0), ((-1)*point(0))*reference_point(1), ((-1)*point(0))*reference_point(2);
    }
}
