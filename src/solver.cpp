#include "solver.h"

using namespace Eigen;
using namespace std;

void solver::standardMatch(const Vector2dVector& references, const Vector2dVector& points, const IndexSet& correspondences_set, Isometry2d& Guess, const double &threshold, const int& maxIter)
{
    cout << "::ENTERING standardMatch()::\n";
    Matrix3d H;
    MatrixXd J;
    Vector3d b, linearSolution;
    Vector2d err;

    double error = 0.;
    double chi2 = 0.;
    for(int i=0; i<maxIter; i++)
    {
        for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
        {
            IndexPair p = *it;
            Vector3d reference(0., 0., 1.);
            reference.head(2)= references[p.second];
            Vector3d point(0., 0., 1.);
            point.head(2) = points[p.first];
            point = Guess*point;
            error += (reference-point).squaredNorm();
        }
        error = error/correspondences_set.size();

        b.setZero(3);
        err.setZero(2);
        H.setZero(3,3);
        J.setZero(2,3);
        chi2=0.0;
        for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
        {
            IndexPair p = *it;
            Vector3d reference(0., 0., 1.);
            reference.head(2)= references[p.second];
            Vector3d point(0., 0., 1.);
            point.head(2) = points[p.first];
            reference = Guess*reference;
            err = (point-reference).head(2);
            chi2+=err.transpose()*err;
            J = Jacobian(point);
            H += (J.transpose()*J);
            b += (J.transpose()*err);

        }
        chi2 = chi2/correspondences_set.size();

        cout << i << ") Chi2 = " << chi2 << ", Error = " << error << endl;

        linearSolution.setZero(3);
        linearSolution = H.ldlt().solve(-b);
        Isometry2d tmp = add(Guess, linearSolution);
        Guess = tmp;

        cerr << i << ") Delta_H =\n" << Guess.matrix() << endl;


//		this->cloud_ = (Translation2d(this->pose_(0),this->pose_(1))*Rotation2D<double>(this->pose_(2)))*cloud;
//		indexes = fcpMdistance(2, sC);
    }

    cout << "::EXITING standardMatch()::\n";
}

void solver::standardMatchLM(const Vector2dVector& references, const Vector2dVector& points, const IndexSet& correspondences_set, Eigen::Isometry2d &Guess, const double& epsilon, const int& maxIter){
    cerr << "::ENTERING standardMatchLM()::\n";
    Matrix3d H;
    Matrix3d LMS;
    MatrixXd J;
    Matrix3d Iden = MatrixXd::Identity(3,3);
    Vector3d b, linearSolution, backup;
    Vector2d err;

    Matrix2d references_covariance;
    Matrix2d points_covariance;
    references_covariance.setZero(2, 2);
    points_covariance.setZero(2, 2);

    double error = 0.0;
    double currError = 0.0;
    int countIter = 0;

//    cerr << "Initial Guess = \n" << Guess.matrix() << endl;
    backup = t2V(Guess);

    for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
    {
        IndexPair p = *it;
        Vector3d reference(0., 0., 1.);
        reference.head(2)= references[p.second];
        Vector3d point(0., 0., 1.);
        point.head(2) = points[p.first];
        reference = Guess*reference;
        error += (reference-point).squaredNorm();
    }
    error = error/correspondences_set.size();

    double chi2;
    double lambda = 0.1;
    while(abs(currError-error)>epsilon && countIter<maxIter)
    {
        currError = error;
        chi2 = 0.0;
        b.setZero(3);
        err.setZero(2);
        H.setZero(3,3);
        J.setZero(2,3);

        for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
        {
            IndexPair p = *it;
            Vector3d reference(0., 0., 1.);
            reference.head(2)= references[p.second];
            Vector3d point(0., 0., 1.);
            point.head(2) = points[p.first];
            reference = Guess*reference;
            err = (point-reference).head(2);
            chi2+=err.transpose()*err;
            J = Jacobian(point);
            H += (J.transpose()*J);
            b += (J.transpose()*err);

        }
        chi2 = chi2/correspondences_set.size();
        cerr << "Chi2 = " << chi2 << "; ";

        countIter = 0;
        do
        {
            LMS.setZero(3,3);
            linearSolution.setZero(3);
            LMS = H+(lambda*Iden);
            linearSolution = LMS.ldlt().solve(-b);
            Isometry2d tmp = add(Guess,linearSolution);
            cerr << "Linear Solution = \n" << linearSolution << endl;
            Guess = tmp;

            error = 0.;
            for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
            {
                IndexPair p = *it;
                Vector3d reference(0., 0., 1.);
                reference.head(2)= references[p.second];
                Vector3d point(0., 0., 1.);
                point.head(2) = points[p.first];
                reference = Guess*reference;
                error += (reference-point).squaredNorm();
            }
            error = error/correspondences_set.size();

            if(error<currError)
            {
                lambda = lambda/2;
//                backup = t2V(Guess);
                countIter--;
//                cerr << "[GOOD STEP]: Lambda = " << lambda << "; Error = " << error << "; iter = " << countIter << endl;
//                cerr << "\tGuess = \n" << Guess.matrix() << endl;
            }
            else
            {
                lambda = lambda*4;
//                Guess = v2T(backup);
                Guess = v2T(sub(v2T(linearSolution), Guess));
                countIter++;
//                cerr << "[BAD STEP]: Lambda = " << lambda << "; Error = " << error << "; iter = " << countIter << endl;
//                cerr << "\tGuess = \n" << Guess.matrix() << endl;
            }

        }while(countIter<maxIter && countIter>0);
    }
//    cerr << "New Guess = \n" << Guess.matrix() << endl;
    cerr << "::EXITING standardMatchLM()::\n";
}

void solver::generalizedMatchLM(const Vector2dVector& references, const Vector2dVector& points, const IndexSet& correspondences_set, Eigen::Isometry2d &Guess, const double& epsilon, const int& maxIter){
    cerr << "::ENTERING generalizedMatchLM()::\n";
    Matrix3d H;
    Matrix3d LMS;
    MatrixXd J;
    Matrix3d Iden = MatrixXd::Identity(3,3);
    Vector3d b, linearSolution, backup;
    Vector2d err;

    Matrix2d references_covariance;
    Matrix2d points_covariance;
    Matrix2d information_Matrix;

    double error = 0.0;
    double currError = 0.0;
    int countIter = 0;

//    cerr << "Initial Guess = \n" << Guess.matrix() << endl;
    backup = t2V(Guess);

    for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
    {
        IndexPair p = *it;
        Vector3d reference(0., 0., 1.);
        reference.head(2)= references[p.second];

        Vector3d point(0., 0., 1.);
        point.head(2) = points[p.first];
        reference = Guess*reference;
        error += (reference-point).squaredNorm();
    }
    error = error/correspondences_set.size();

    double chi2;
    double lambda = 0.1;
    while(abs(currError-error)>epsilon && countIter<maxIter)
    {
        currError = error;
        chi2 = 0.0;
        b.setZero(3);
        err.setZero(2);
        H.setZero(3,3);
        J.setZero(2,3);

        for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
        {
            IndexPair p = *it;
            references_covariance.setZero(2, 2);
            points_covariance.setZero(2, 2);
            Vector3d reference(0., 0., 1.);
            reference.head(2)= references[p.second];
            pointCovariance(references[p.second], references, references_covariance, 20, .7);
//            std::cerr << "Ref. Covariance = \n" << references_covariance << std::endl;
            Vector3d point(0., 0., 1.);
            point.head(2) = points[p.first];
            pointCovariance(points[p.first], points, points_covariance, 20, .7);
//            std::cerr << "Covariance = \n" << points_covariance << std::endl;
            reference = Guess*reference;
            err = (point-reference).head(2);
            chi2+=err.transpose()*err;
            J = Jacobian(point);
            information_Matrix = (points_covariance + (Guess.linear()*references_covariance*(Guess.linear().inverse()))).inverse();
            H += (J.transpose()*information_Matrix*J);
            b += (J.transpose()*information_Matrix*err);

        }
        chi2 = chi2/correspondences_set.size();
        cerr << "Chi2 = " << chi2 << "; ";

        countIter = 0;
        do
        {
            LMS.setZero(3,3);
            linearSolution.setZero(3);
            LMS = H+(lambda*Iden);
            linearSolution = LMS.ldlt().solve(-b);
            Isometry2d tmp = add(Guess,linearSolution);
//            cerr << "Linear Solution = \n" << linearSolution << endl;
            Guess = tmp;

            error = 0.;
            for(IndexSetIterator it=correspondences_set.begin(); it!=correspondences_set.end(); ++it)
            {
                IndexPair p = *it;
                Vector3d reference(0., 0., 1.);
                reference.head(2)= references[p.second];
                Vector3d point(0., 0., 1.);
                point.head(2) = points[p.first];
                reference = Guess*reference;
                error += (reference-point).squaredNorm();
            }
            error = error/correspondences_set.size();

            if(error<currError)
            {
                lambda = lambda/2;
                backup = t2V(Guess);
                countIter--;
//                cerr << "[GOOD STEP]: Lambda = " << lambda << "; Error = " << error << "; iter = " << countIter << endl;
//                cerr << "\tGuess = \n" << Guess.matrix() << endl;
            }
            else
            {
                lambda = lambda*4;
                Guess = v2T(backup);
                countIter++;
//                cerr << "[BAD STEP]: Lambda = " << lambda << "; Error = " << error << "; iter = " << countIter << endl;
//                cerr << "\tGuess = \n" << Guess.matrix() << endl;
            }

        }while(countIter<maxIter && countIter>0);
    }
//    cerr << "New Guess = \n" << Guess.matrix() << endl;
    cerr << "::EXITING standardMatchLM()::\n";
}

void solver::pointCovariance(const Eigen::Vector2d point, const Vector2dVector& points, Eigen::Matrix2d& covariance, const int& neighbours, const double& belief){
    MatrixXd Neighbourhood;
    Matrix2d V;
    NeighbourDistance N;

    covariance.setZero(2,2);
    V.setZero(2,2);
    Neighbourhood.setZero(2,neighbours);

    for(size_t i=0; i<points.size(); ++i){
        Eigen::Vector2d v = points[i];
        double error = (v-point).norm();
        NeighbourEntry ngh = NeighbourEntry();
        ngh.index = i;
        ngh.error = error;
        N.push(ngh);
    }

    // Remove the first entry, which is the point itself
    N.pop();

    for(int i=0; i<neighbours; ++i){
        NeighbourEntry ngh = N.top();
        N.pop();
        Neighbourhood.col(i) = points[ngh.index];
    }

    MatrixXd Neighbourhood_centered = Neighbourhood.colwise() - Neighbourhood.rowwise().mean();
//	centered.transposeInPlace();

//	covariance = (tmpMtrx*tmpMtrx.transpose())*(1.0/(nghbrhdDm1));
    covariance = (Neighbourhood_centered*Neighbourhood_centered.transpose())*(1.0/(neighbours));
//	covariance = centered.adjoint()*centered;
//	EigenSolver<MatrixXd> eigSolv;
    SelfAdjointEigenSolver<MatrixXd> eigSolv;
//	eigSolv.compute(covariance, true);
    eigSolv.compute(covariance, ComputeEigenvectors);
    if (eigSolv.info() != Success) abort();
//	V = eigSolv.pseudoEigenvectors();
    V = eigSolv.eigenvectors();
//    std::cerr << "V = \n" << V << std::endl;

    covariance = V*DiagonalMatrix<double,2>(belief,1.0)*V.transpose();

}

void solver::pointNormal(const Vector2d point, const Vector2dVector &points, Vector2d &normal_vector, const int &neighbours){
    Matrix2d covariance;
    MatrixXd Neighbourhood;
    MatrixXd V;
    NeighbourDistance N;

    normal_vector.setZero(2);
    covariance.setZero(2,2);
    Neighbourhood.setZero(2, neighbours);
    V.setZero(2,2);

    for(size_t i=0; i<points.size(); ++i){
        Vector2d v = points[i];
        double error = (v-point).norm();
        NeighbourEntry ngh = NeighbourEntry();
        ngh.index = i;
        ngh.error = error;
        N.push(ngh);
    }

    // Remove the first entry, which is the point itself
    N.pop();

    for(int i=0; i<neighbours; ++i){
        NeighbourEntry ngh = N.top();
        N.pop();
        Neighbourhood.col(i) = points[ngh.index];
    }

    MatrixXd Neighbourhood_centered = Neighbourhood.colwise() - Neighbourhood.rowwise().mean();
    covariance = (Neighbourhood_centered*Neighbourhood_centered.transpose())*(1.0/(neighbours));
    SelfAdjointEigenSolver<MatrixXd> eigSolv;
    eigSolv.compute(covariance, ComputeEigenvectors);
    if (eigSolv.info() != Success) abort();
    V = eigSolv.eigenvectors();
    normal_vector = V.col(0);

}
